/*
 * Priority.cpp
 *
 *  Created on: Feb 6, 2016
 *      Author: thelenn1
 */

#include <boost/lexical_cast.hpp>
#include "Priority.h"

Priority::Priority(pugi::xml_node node)
{
	// TODO Auto-generated constructor stub
	for (pugi::xml_attribute attr: node.attributes())
	{
		if (std::string(attr.name()) == std::string("high"))
			data.high = std::string(attr.value());
		if (std::string(attr.name()) == std::string("low"))
			data.low = std::string(attr.value());
	}

}

Priority::~Priority()
{
	// TODO Auto-generated destructor stub
}

PriorityData Priority::getPriorityData()
{
	return data;
}
