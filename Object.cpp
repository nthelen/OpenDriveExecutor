/*
 * Object.cpp
 *
 *  Created on: Feb 22, 2016
 *      Author: thelenn1
 */

#include <boost/lexical_cast.hpp>

#include "Object.h"


Object::Object(pugi::xml_node node)
{
	data.length = -1;
	data.width= -1;
			data.radius = -1;
	// TODO Auto-generated constructor stub
	for (pugi::xml_attribute attr: node.attributes())
	{
		if (std::string(attr.name()) == std::string("type"))
			data.type = std::string(attr.value());
		if (std::string(attr.name()) == std::string("name"))
			data.name = std::string(attr.value());
		if (std::string(attr.name()) == std::string("id"))
			data.id = std::string(attr.value());
		if (std::string(attr.name()) == std::string("s"))
			data.s = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("t"))
			data.t = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("zOffset"))
			data.zOffset = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("validLength"))
			data.validLength = boost::lexical_cast<double>(attr.value());

		if (std::string(attr.name()) == std::string("orientation"))
		{
			if (std::string(attr.value()) == std::string("+"))
				data.orientation = ObjectOrientation::Positive;
			if (std::string(attr.value()) == std::string("-"))
				data.orientation = ObjectOrientation::Negative;
			if (std::string(attr.value()) == std::string("none"))
				data.orientation = ObjectOrientation::ObjectNone;
		}

		if (std::string(attr.value()) == std::string("length"))
			data.length = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.value()) == std::string("width"))
			data.width = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.value()) == std::string("radius"))
			data.radius = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.value()) == std::string("height"))
			data.height = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.value()) == std::string("hdg"))
			data.hdg = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.value()) == std::string("pitch"))
			data.pitch = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.value()) == std::string("roll"))
			data.roll = boost::lexical_cast<double>(attr.value());
	}
	// TODO Auto-generated constructor stub

}

Object::~Object() {
	// TODO Auto-generated destructor stub
}


const ObjectData& Object::getData() const
{
	return data;
}
