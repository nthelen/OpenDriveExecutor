/*
 * Junction.h
 *
 *  Created on: Feb 6, 2016
 *      Author: thelenn1
 */

#ifndef JUNCTION_H_
#define JUNCTION_H_

#include <vector>
#include <memory>
#include <string>
#include "XML/pugixml.hpp"
#include "priority.h"
#include "connection.h"
#include "controller.h"
#include <utility>

struct JunctionData
{
	std::vector< std::shared_ptr<Priority > > priorities;
	std::vector< std::shared_ptr<Connection> > connections;
	std::vector< std::shared_ptr<Controller> > controllers;

	std::string name;
	std::string id;

};
class Junction
{
public:
	Junction(pugi::xml_node node);

	std::vector<std::string> getConnectingRoads(std::string) const;
	const JunctionData& getData() const { return data; }
protected:

	JunctionData data;


};

#endif /* JUNCTION_H_ */
