/*
 * Roads.h
 *
 *  Created on: Feb 21, 2016
 *      Author: thelenn1
 */

#ifndef ROADS_MAINNODE_H_
#define ROADS_MAINNODE_H_

#include <vector>
#include <memory>
#include <string>
#include <utility>

#include "Roads/Road.h"
#include "Roads/Lane.h"
#include "XML/pugixml.hpp"
#include "Junction.h"
#include "Controller.h"
#include "Roads/Signal.h"
#include "Object.h"
#include "Trees/QuadTree.h"
#include "Graph/RoadNode.h"

	class MainNode
	{
	private:
		void searchOpenDrive(pugi::xml_node node);
		void mainParse(pugi::xml_node node);
	protected:
		std::map< std::string,std::shared_ptr<Road> > roads;
		std::map< std::string,std::shared_ptr<Junction> > junctions;
		std::map< std::string,std::shared_ptr<Controller> > controllers;
		std::map< std::string, std::shared_ptr<Signal> > signals;
		std::map< std::string, std::shared_ptr<Object> > objects;

		std::function<ObjectData(std::string)> o;
		std::function<SignalData(std::string)> s;


		QuadTree<TreeData>* quadTree;

		//std::map< std::string,std::pair<std::vector<RoadNode>,std::vector<RoadNode> > >  roadNodes;
		std::map< std::string,std::vector<std::string> >  roadNodes;
		void buildTree(OpenDriveMath::Box range);
		void createConnectors();

	public:
		MainNode(pugi::xml_node node);
		MainNode(std::string fileName);

		const RoadData& getRoad(std::string);
		ObjectData findObject(std::string);
		SignalData findSignal(std::string);
		RoadData findRoad(std::string);

		void getSignals(pugi::xml_node node);
		void getObjects(pugi::xml_node node);

		UserRoadData laneToRoad(UserLaneData data) const;
		UserRoadData positionToRoad(UserPointData data);
		UserRoadData routeToRoad(UserRouteData path);
		UserPointData roadToPoint(UserRoadData path);
		UserLaneData roadToLane(UserRoadData path);
		UserRouteData roadToRoute(UserRoadData roadData,OpenDrivePath path);

	};

#endif /* ROADS_MAINNODE_H_ */
