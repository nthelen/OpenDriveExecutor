/*
 * Controller.h
 *
 *  Created on: Feb 6, 2016
 *      Author: thelenn1
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <vector>
#include <memory>
#include "XML/pugixml.hpp"
#include <utility>
#include "control.h"

struct ControllerData
{
	std::string id;
	std::string name;
	int sequence;
	std::vector<std::shared_ptr<Control> > controls;
};

class Controller
{
public:
	const ControllerData& getData() const { return data; }
	Controller(pugi::xml_node node);
	virtual ~Controller();
protected:
	ControllerData data;
};

#endif /* CONTROLLER_H_ */
