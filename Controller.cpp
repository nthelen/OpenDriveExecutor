/*
 * Controller.cpp
 *
 *  Created on: Feb 6, 2016
 *      Author: thelenn1
 */
#include <memory>
#include <boost/lexical_cast.hpp>

#include "Controller.h"


Controller::Controller(pugi::xml_node node)
{
	// TODO Auto-generated constructor stub
	for (pugi::xml_attribute attr: node.attributes())
	{
		if (std::string(attr.name()) == std::string("id"))
			data.id = std::string(attr.value());
		if (std::string(attr.name()) == std::string("name"))
			data.name = std::string(attr.value());
		if (std::string(attr.name()) == std::string("sequence"))
			data.sequence = boost::lexical_cast<int>(attr.value());
	}
	for (pugi::xml_node child: node.children())
	{
		if(child.name() == std::string("control"))
		{
			Control* control = new Control(child);
			data.controls.push_back(std::shared_ptr<Control>(control));
		}
	}
}

Controller::~Controller() {
	// TODO Auto-generated destructor stub
}

