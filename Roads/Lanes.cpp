/*
 * Lane.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#include "Lane.h"
#include "Lanes.h"

Lanes::Lanes(pugi::xml_node node)
{
	for (pugi::xml_node child : node.children())
	{
		if (std::string(child.name()) == std::string("center"))
		{
			AddLanes(child,LanePosition::Center);
		}
		if (std::string(child.name()) == std::string("left"))
		{
			AddLanes(child,LanePosition::Left);
		}
		if (std::string(child.name()) == std::string("right"))
		{
			AddLanes(child,LanePosition::Right);
		}
		if (std::string(child.name())== std::string("Offset"))
		{
			HandleOffset(child);
		}
		if (std::string(child.name()) == std::string("Section"))
		{
			HandleSection(child);
		}
	}
}

bool Lanes::HandleOffset(pugi::xml_node node)
{
	for (pugi::xml_attribute attr : node.attributes()) {
		if (std::string(attr.name()) == std::string("s"))
			data.offset = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("a"))
			data.a = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("b"))
			data.b = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("c"))
			data.c = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("d"))
			data.c = boost::lexical_cast<double>(attr.value());
	}
	return true;
}

bool Lanes::HandleSection(pugi::xml_node node)
{
	//NTT what is this?
	return true;

}

std::pair<double,double> Lanes::getLength(double as)
{
	std::pair<double,double> returnValue(0,0);
	for(auto lane : data.RightLanes)
	{
		returnValue.first += lane->getLength(as-data.s);
	}

	for(auto lane : data.LeftLanes)
	{

		returnValue.second += lane->getLength(as-data.s);
	}

	return returnValue;
}



bool Lanes::AddLanes(pugi::xml_node node, LanePosition position)
{
	for (pugi::xml_node node : node.children())
	{
		if (node.name() == std::string("lane"))
		{
			if(position == LanePosition::Center)
			{
				data.CenterLanes.push_back(std::shared_ptr<Lane>(new Lane(node)));
			}
			if(position == LanePosition::Left)
			{
				data.LeftLanes.push_back(std::shared_ptr<Lane>(new Lane(node)));
			}
			if(position == LanePosition::Right)
			{
				data.RightLanes.push_back(std::shared_ptr<Lane>(new Lane(node)));
			}
		}
	}
	return true;
}

double Lanes::getOffset(double as)
{
	double ds = as-data.s;
	return data.a+data.b*ds+data.c*ds*ds+data.d*ds*ds*ds;
}

UserLaneData Lanes::getLaneDataFromRoadData(UserRoadData road)
{
	double currentLength=getOffset(road.sPosition);
	double newLength=0;
	UserLaneData laneData;

	for(auto lane : data.RightLanes)
	{
		newLength = lane->getLength(road.sPosition-data.s);

		if(road.roadOffset >= currentLength && road.roadOffset<= newLength)
		{
			laneData.laneNumber = lane->getData().id;
			laneData.laneOffset = road.roadOffset-currentLength;
			laneData.roadId = road.road;
			laneData.roadOffset = road.sPosition;
			return laneData;
		}
		currentLength += newLength;
	}

	currentLength=getOffset(road.sPosition);
	newLength=0;

	for(auto lane : data.LeftLanes)
	{
		newLength = lane->getLength(road.sPosition);

		if(road.roadOffset >= currentLength && road.roadOffset<= newLength)
		{
			laneData.laneNumber = lane->getData().id;
			laneData.laneOffset = road.roadOffset-currentLength;
			laneData.roadId = road.road;
			laneData.roadOffset = road.sPosition;
			return laneData;
		}
		currentLength += newLength;
	}
	throw std::runtime_error("Nothing in that range");
}

UserRoadData Lanes::getRoadDataFromLaneData(UserLaneData lane) const
{
	UserRoadData roadData = UserRoadData();

	roadData.road = lane.roadId;

	const std::vector< std::shared_ptr<Lane> > *lanes;

	//NTT: What about center lanes?
	if(lane.laneNumber > 0)
		lanes = &data.RightLanes;
	if(lane.laneNumber < 0)
		lanes = &data.LeftLanes;

	for(auto l : *lanes)
	{
		roadData.roadOffset += l->getLength(lane.roadOffset);
	}

	roadData.roadOffset += lane.laneOffset;
	roadData.sPosition = lane.roadOffset;

	return roadData;
}
