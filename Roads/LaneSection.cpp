/*
 * LaneSection.cpp
 *
 *  Created on: Feb 12, 2016
 *      Author: thelenn1
 */

#include "LaneSection.h"
#include "../XML/pugixml.hpp"
#include <boost/lexical_cast.hpp>


LaneSection::LaneSection(pugi::xml_node node)
{
	for (pugi::xml_attribute attr : node.attributes())
	{
		if (std::string(attr.name()) == std::string("s"))
			s = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("singleSide"))
			singleSide = boost::lexical_cast<bool>(attr.value());
	}
}

LaneSection::~LaneSection()
{
	// TODO Auto-generated destructor stub
}

