/*
 * Speed.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#include "Speed.h"
#include <boost/lexical_cast.hpp>

Speed::Speed(pugi::xml_node node)
{
	for (pugi::xml_attribute attr: node.attributes())
	{
		if(std::string(attr.name()) == std::string("speed"))
			data.speed = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) ==std::string("b"))
			data.units = boost::lexical_cast<std::string>(attr.value());
		//TODO convert units
	}
}

Speed::~Speed()
{
	// TODO Auto-generated destructor stub
}
