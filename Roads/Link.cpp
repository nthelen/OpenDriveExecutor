/*
 * Link.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#include "Link.h"

Link::Link(pugi::xml_node node)
{
	for (pugi::xml_attribute attr: node.attributes())
	{
		if(attr.value() == std::string("elementType"))
		{
			if(std::string(attr.value()) == std::string("road"))
				data.elementType = ElementTypes::RoadElement;
			if(std::string(attr.value()) == std::string("junction"))
				data.elementType = ElementTypes::JunctionElement;
		}
		if(attr.value() == std::string("contactPoint"))
		{
			if(std::string(attr.value()) == std::string("start"))
				data.contactPoint = ContactPoints::Start;
			if(std::string(attr.value()) == std::string("end"))
				data.contactPoint = ContactPoints::End;
		}
		if(std::string(attr.value()) == std::string("elementId"))
			data.elementId = attr.value();
	}
}
