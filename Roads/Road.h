/*
 * Road.h
 *
 *  Created on: Feb 7, 2016
 *      Author: thelenn1
 */

#ifndef ROADS_ROAD_H_
#define ROADS_ROAD_H_

#include <memory>
#include <map>
#include <string>
#include <vector>

#include "../Geometry/Geometry.h"
#include "../Geometry/LateralProfile.h"
#include "../Geometry/Elevation.h"
#include "Signal.h"
#include "../Object.h"
#include "../Trees/QuadTreeItem.h"
#include <utility>
#include "Link.h"
#include "Speed.h"
#include "../Junction.h"
#include "Lanes.h"

//typedef std::pair< std::string,double> RoadNode;

class TreeData;
class Speed;

class RoadData
{
	public:
	Link predecessor;
	Link successor;
	Speed speed;
	std::map< double, std::shared_ptr<Lanes> > lanes;

	std::map< double, std::shared_ptr<Geometry> > geometries;
	std::map< double, std::shared_ptr<LateralProfile> > lateralProfiles;
	std::map< double, std::shared_ptr<Elevation> > elevations;
	std::map< double, RoadSpecificSignalData > signals;
	std::map< double, RoadSpecificObjectData > objects;

	std::string id;
	std::string name;
	std::string junction;
	double length;
	bool isJunction;
};

class Road
{
	public:
		enum NodeDirection { Front=0, Back=1};
		Road(pugi::xml_node node,std::function< ObjectData(std::string)> findObject, std::function< SignalData(std::string)> findSignal);
		int getNextRoad();
		int getPreviousRoad();
		double getElevation(double s, double t);
		double getSuperElevation(double s, double t);
		OpenDriveMath::Point getCoordinates();
		double getSpeed();
		double getLength();
		const RoadData& getData() const {return data;};
		std::vector<TreeData> getGeometryAreas();
		UserPointData getPosition(double s, double t);
		UserLaneData getLaneData(UserRoadData userRoadData);
		const RoadNode& getNode(Road::NodeDirection dir) const;
		std::pair<std::vector<RoadNode>,std::vector<RoadNode> > connectNodes(
				std::function< const Junction& (std::string)> getJunction,
				std::function< const Road& (std::string)> getRoad);

		Road();
		std::pair<double,double> getLaneLength(double s);

	private:
		bool readElevationProfile(pugi::xml_node node);
		bool readLateralProfile(pugi::xml_node node);
		bool readLanes(pugi::xml_node node);
		bool readObjects(pugi::xml_node node, std::function< ObjectData(std::string)> findObject);
		bool readSignals(pugi::xml_node node, std::function< SignalData(std::string)> findSignal);
		bool readPlanview(pugi::xml_node node);
		bool readType(pugi::xml_node node);
		bool readLink(pugi::xml_node node);
		void createNodes();

	RoadData data;
	RoadNode startNode;
	RoadNode endNode ;
};



#endif /* ROADS_ROAD_H_ */
