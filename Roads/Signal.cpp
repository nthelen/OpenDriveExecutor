/*
 * Signal.cpp
 *
 *  Created on: Feb 12, 2016
 *      Author: thelenn1
 */

#include "Signal.h"
#include <boost/lexical_cast.hpp>

Signal::Signal(pugi::xml_node node)
{
	for (pugi::xml_attribute attr : node.attributes())
	{
		if (std::string(attr.name()) == std::string("s"))
			data.s = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("t"))
			data.t = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("id"))
			data.id = std::string(attr.value());
		if (std::string(attr.name()) == std::string("name"))
			data.name = std::string(attr.value());
		if (std::string(attr.name()) == std::string("dynamic"))
		{
			if(attr.value() == std::string("yes"))
				data.dynamic = Dynamic::Yes;
			if(attr.value() == std::string("no"))
				data.dynamic = Dynamic::No;
		}
		if (std::string(attr.name()) == "orientation")
		{
			if(std::string(attr.value()) == std::string("+"))
				data.orientation = Orientation::Plus;
			if(std::string(attr.value())== std::string("_"))
				data.orientation = Orientation::Minus;
			if(std::string(attr.value()) == std::string("none"))
				data.orientation = Orientation::Both;
		}
		if (std::string(attr.name()) == std::string("zOffset"))
			data.zOffset = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("country"))
			data.country = std::string(attr.value());
		if (std::string(attr.name()) == std::string("type"))
			data.type = std::string(attr.value());
		if (std::string(attr.name()) == std::string("subtype"))
			data.subtype = std::string(attr.value());
		if (std::string(attr.name()) == std::string("value"))
			data.value = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("unit"))
			data.unit = std::string(attr.value());
		if (std::string(attr.name()) == std::string("height"))
			data.height = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("width"))
			data.width = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("text"))
			data.text = std::string(attr.value());
		if (std::string(attr.name()) == std::string("hOffset"))
			data.hOffset = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("pitch"))
			data.pitch = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("roll"))
			data.roll = boost::lexical_cast<double>(attr.value());
	}

	for (pugi::xml_node child: node.children())
	{
		if (std::string(child.name()) == std::string("dependency"))
			data.signalDependencies.push_back(handleSignalDependency(node));
		if (std::string(child.name()) == std::string("validity"))
			data.signalValidities.push_back(handleSignalValidity(node));
	}

}

SignalValidity Signal::handleSignalValidity(pugi::xml_node node)
{
	int fromLane;
	int toLane;
	for (pugi::xml_attribute attr : node.attributes())
	{
		if (std::string(attr.name()) == std::string("fromLane"))
			fromLane = boost::lexical_cast<int>(attr.value());
		if (std::string(attr.name()) == std::string("toLane"))
			toLane = boost::lexical_cast<int>(attr.value());
	}
	return SignalValidity(fromLane, toLane);
}

SignalDependency Signal::handleSignalDependency(pugi::xml_node node)
{
	std::string id;
	std::string type;
	for (pugi::xml_attribute attr : node.attributes())
	{
		if (std::string(attr.name()) == std::string("id"))
			id = std::string(attr.value());
		if (std::string(attr.name()) == std::string("type"))
			type = std::string(attr.value());
	}
	return SignalDependency(id, type);
}


Signal::~Signal() {
	// TODO Auto-generated destructor stub
}

