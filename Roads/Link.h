/*
 * Link.h
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#ifndef LINK_H_
#define LINK_H_

#include <vector>
#include <memory>
#include "../Roads/Lane.h"
#include "../XML/pugixml.hpp"
#include <utility>

enum ContactPoints { Start = 0, End = 1 };
enum ElementTypes { RoadElement = 0, JunctionElement = 1 };


struct LinkData
{
	ElementTypes elementType;
	std::string elementId;
	ContactPoints contactPoint;
};

class Link
{
public:
	Link(pugi::xml_node);
	Link(){};
	const LinkData& getData() const {return data;};
protected:
	LinkData data;
};

#endif /* LINK_H_ */
