/*
 * LaneSection.h
 *
 *  Created on: Feb 12, 2016
 *      Author: thelenn1
 */

#ifndef ROADS_LANESECTION_H_
#define ROADS_LANESECTION_H_

#include <boost/lexical_cast.hpp>

#include "../XML/pugixml.hpp"

class LaneSection
{
public:
	LaneSection(pugi::xml_node node);
	virtual ~LaneSection();

	double s;
	bool singleSide;
};

#endif /* ROADS_LANESECTION_H_ */
