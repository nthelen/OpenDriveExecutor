/*
 * Road.cpp
 *
 *  Created on: Jan 24, 2016
 *      Author: thelenn1
 */
#include "Road.h"
#include <vector>
#include <Map>
#include <Memory>
#include "Link.h"
#include "../Geometry/Geometry.h"
#include "Speed.h"
#include "../Math/Point.h"
#include "../Math/LinearLine.h"
#include <functional>
#include "../Geometry/Geometry.h"
#include "../Geometry/Elevation.h"
#include "../Geometry/Arc.h"
#include "../Geometry/Spiral.h"
#include "../Geometry/Arc.h"
#include "../Geometry/Line.h"
#include "../Geometry/ParametricCubic.h"
#include "../Geometry/SuperElevation.h"
#include "../Geometry/Crossfall.h"
#include "../Geometry/Shape.h"
#include "../Trees/QuadTreeItem.h"

std::map<std::string,std::function<bool(pugi::xml_node)> > functionCall;

Road::Road(pugi::xml_node node,std::function< ObjectData(std::string)> findObject,
		std::function< SignalData(std::string)> findSignal)
{
	functionCall["link"] = std::bind(&Road::readLink, this, std::placeholders::_1);
	functionCall["type"] = std::bind(&Road::readType, this, std::placeholders::_1);
	functionCall["planView"] = std::bind(&Road::readPlanview, this, std::placeholders::_1);
	functionCall["elevationProfile"] = std::bind(&Road::readElevationProfile, this, std::placeholders::_1);
	functionCall["lateralProfile"] = std::bind(&Road::readLateralProfile, this, std::placeholders::_1);
	functionCall["lanes"] = std::bind(&Road::readLanes, this, std::placeholders::_1);
	functionCall["objects"] = std::bind(&Road::readObjects,this, std::placeholders::_1, findObject);
	functionCall["signals"] = std::bind(&Road::readSignals,this, std::placeholders::_1, findSignal);

	for (pugi::xml_attribute attr: node.attributes())
	{
		if (std::string(attr.name())==std::string("id"))
			data.id = attr.value();
		if (std::string(attr.name())==std::string("junction"))
			data.junction = attr.value();
		if (std::string(attr.name())==std::string("name"))
			data.name = attr.value();
		if (std::string(attr.name())==std::string("length"))
			data.length = boost::lexical_cast<double>(attr.value());
	}

	for (pugi::xml_node child: node.children())
	{
		functionCall[std::string(child.name())](child);
	}

	data.isJunction = boost::lexical_cast<int>(data.junction) > 0;

	createNodes();
}


bool Road::readLink(pugi::xml_node node)
{
	for (pugi::xml_node child: node.children())
	{
		if(std::string(child.name())==std::string("predecessor"))
			data.predecessor = Link(child);
		if(std::string(child.name())==std::string("successor"))
			data.successor = Link(child);
	}
	return true;
}

bool Road::readType(pugi::xml_node node)
{
	for (pugi::xml_node child: node.children())
	{
		if(std::string(child.name())==std::string("speed"))
			data.speed = Speed(child);
	}
	return true;
}

bool Road::readPlanview(pugi::xml_node node)
{
	for (pugi::xml_node child1: node.children())
	{
		for(pugi::xml_node child: child1.children())
		{
			if (std::string(child.name())==std::string("line"))
			{
				Line* line = new Line(child);
				data.geometries[line->getS()] = std::shared_ptr<Geometry>(line);
			}
			if (std::string(child.name())==std::string("arc"))
			{
				Arc* arc = new Arc(child);
				data.geometries[arc->getS()] = std::shared_ptr<Geometry>(arc);
			}
			if (std::string(child.name())==std::string("poly3"))
			{
				ParametricCubic* parametriccubic = new ParametricCubic(child);
				data.geometries[parametriccubic->getS()] = std::shared_ptr<Geometry>(parametriccubic);
			}
			if (std::string(child.name())==std::string("spiral"))
			{
				Spiral* spiral = new Spiral(child);
				data.geometries[spiral->getS()] = std::shared_ptr<Geometry>(spiral);
			}
		}
	}
	return true;
}

bool Road::readElevationProfile(pugi::xml_node node)
{
	for (pugi::xml_node child: node.children())
	{
		if (std::string(child.name())==std::string("elevation"))
		{
			Elevation* elevation = new Elevation(child);
			data.elevations[elevation->getData().s]= std::shared_ptr<Elevation>(elevation);
		}
	}
	return true;
}

bool Road::readLateralProfile(pugi::xml_node node)
{
	for (pugi::xml_node child: node.children())
	{
		if (std::string(child.name())==std::string("superelevation"))
		{
			SuperElevation* superelevation = new SuperElevation(child);
			data.lateralProfiles[superelevation->getS()] = std::shared_ptr<LateralProfile>(superelevation);
		}
		if (std::string(child.name())==std::string("crossfall"))
		{
			Crossfall* crossfall = new Crossfall(child);
			data.lateralProfiles[crossfall->getS()] = std::shared_ptr<LateralProfile>(crossfall);
		}
		if (std::string(child.name())==std::string("shape"))
		{
			Shape* shape = new Shape(child);
			data.lateralProfiles[shape->getS()] = std::shared_ptr<LateralProfile>(shape);
		}
	}
	return true;
}

bool Road::readLanes(pugi::xml_node node)
{
	for (pugi::xml_node child: node.children())
	{
		if(std::string(child.name()) ==std::string("laneSection"))
		{
			double sPosition=0;

			for(auto attr : child.attributes())
			{
				if (std::string(attr.name()) == std::string("s")) sPosition = boost::lexical_cast<double>(attr.value());
			}

			data.lanes[sPosition] = std::make_shared<Lanes>(child);
		}
	}
	return true;
}


bool Road::readObjects(pugi::xml_node node, std::function< ObjectData(std::string)> findObject)
{
	if(!node.empty())
	{
		for (pugi::xml_node child: node.children())
		{
			std::string objectId;
			if (std::string(child.name())==std::string("object"))
			{
				for(auto attr : child.attributes())
				{
					if (std::string(attr.name()) == std::string("id")) objectId = attr.value();
				}
				RoadSpecificObjectData objectData;

				ObjectData object = findObject(objectId);

				objectData.s = object.s;
				objectData.orientation = object.orientation;
				objectData.t = object.t;
				objectData.id = object.id;
				objectData.validLength = object.validLength;
				objectData.zOffset = object.zOffset;

				data.objects[objectData.s] = objectData;
			}
		}
	}
	return true;
}

bool Road::readSignals(pugi::xml_node node, std::function< SignalData(std::string)> findSignal)
{
	for (pugi::xml_node child: node.children())
	{
		if (std::string(child.name())==std::string("signal") || child.name()==std::string("signalReference"))
		{
			double signals;
			std::string signalname;
			double signalt;
			std::string signalid;

			for(auto attr : child.attributes())
			{
				if(std::string(attr.name()) == std::string("s")) signals = boost::lexical_cast<double>(attr.value());
				if(std::string(attr.name()) == std::string("name")) signalname = std::string(attr.value());
				if(std::string(attr.name()) == std::string("t")) signalt = boost::lexical_cast<double>(attr.value());
				if(std::string(attr.name()) == std::string("id")) signalid = std::string(attr.value());
			}

			RoadSpecificSignalData roadSignal;

			//SignalData signal = findSignal(signalid);

			roadSignal.s = signals;
			roadSignal.name = signalname;
			roadSignal.t = signalt;
			roadSignal.id = signalid;

			data.signals[roadSignal.s] = roadSignal;
		}
	}
	return true;
}

const RoadNode& Road::getNode(Road::NodeDirection dir) const
{
	if (dir == Road::NodeDirection::Front)
	{
		return startNode;
	}
	else if(dir ==  Road::NodeDirection::Back)
	{
		return endNode;
	}
	else throw std::runtime_error("Invalid node direction");
}

std::pair<std::vector<RoadNode>,std::vector<RoadNode> > Road::connectNodes(std::function< const Junction& (std::string)> getJunction,std::function< const Road& (std::string)> getRoad)
{
	std::vector<RoadNode> startNodes;
	std::vector<RoadNode> endNodes;

	//connect the start and end nodes
	//startNodes.push_back({data.id,data.length});
	//endNodes.push_back({data.id,0});

	if(data.predecessor.getData().elementType == ElementTypes::JunctionElement)
	{
		std::string temp;
		std::vector<std::string> roads = getJunction(data.predecessor.getData().elementId).getConnectingRoads(data.id);

		for(int i=0; i<roads.size();i++)//data.predecessor.getLinkData().elementId).getConnectingRoads(data.id))
		{
			const Road& road = getRoad(roads[i]);
			if(data.predecessor.getData().contactPoint == ContactPoints::Start)
			{
				Road::NodeDirection n = Road::NodeDirection::Front;
				const RoadNode& roadNode = road.getNode(n);
				startNodes.push_back(roadNode);

			}
			if(data.predecessor.getData().contactPoint == ContactPoints::End)
			{
				Road::NodeDirection n = Road::NodeDirection::Back;
				const RoadNode& roadNode = road.getNode(n);
				startNodes.push_back(roadNode);
			}
		}
	}
	else
	{
		if(data.predecessor.getData().contactPoint == ContactPoints::Start)
		{
			Road::NodeDirection n = Road::NodeDirection::Front;
			const RoadNode& roadNode = getRoad(data.predecessor.getData().elementId).getNode(n);
			startNodes.push_back(roadNode);
		}
		if(data.predecessor.getData().contactPoint == ContactPoints::End)
		{
			Road::NodeDirection n = Road::NodeDirection::Back;
			const RoadNode& roadNode = getRoad(data.predecessor.getData().elementId).getNode(n);
			startNodes.push_back(roadNode);		}
	}
	//NTT Clean up later
	////////////////////////////////////////////////////////////////////////
	if(data.successor.getData().elementType == ElementTypes::JunctionElement)
	{
		std::vector<std::string> roads = getJunction(data.successor.getData().elementId).getConnectingRoads(data.id);

		for(int i=0; i<roads.size();i++)//data.predecessor.getLinkData().elementId).getConnectingRoads(data.id))
		{
			const Road& road = getRoad(roads[i]);
			if(data.successor.getData().contactPoint == ContactPoints::Start)
			{
				const RoadNode& roadNode = road.getNode(Road::NodeDirection::Front);
				endNodes.push_back(roadNode);
			}
			if(data.successor.getData().contactPoint == ContactPoints::End)
			{
				const RoadNode& roadNode = road.getNode(Road::NodeDirection::Back);
				endNodes.push_back(roadNode);
			}
		}
	}
	else
	{
		if(data.successor.getData().contactPoint == ContactPoints::Start)
		{
			Road::NodeDirection n = Road::NodeDirection::Front;
			endNodes.push_back({data.successor.getData().elementId,0});
			const RoadNode& roadNode = getRoad(data.predecessor.getData().elementId).getNode(n);
			endNodes.push_back(roadNode);
		}
		if(data.successor.getData().contactPoint == ContactPoints::End)
		{
			Road::NodeDirection n = Road::NodeDirection::Back;
			endNodes.push_back({data.successor.getData().elementId,getRoad(data.successor.getData().elementId).getData().length});
			const RoadNode& roadNode = getRoad(data.predecessor.getData().elementId).getNode(n);
			endNodes.push_back(roadNode);
		}
	}
	return std::pair<std::vector<RoadNode>,std::vector<RoadNode> >(startNodes, endNodes);
}

void Road::createNodes()
{
	startNode = RoadNode(data.id, 0);
	endNode = RoadNode(data.id, data.length);
}

double Road::getLength()
{
	return data.length;
}

std::vector<TreeData> Road::getGeometryAreas()
{
	std::vector<TreeData> roadNodes;
	for(auto iterator = data.geometries.begin(); iterator != data.geometries.end(); iterator++)
	{
		//auto function =
		std::vector<OpenDriveMath::Box> boxes = (*iterator).second->makeBoxes(std::bind(&Road::getLaneLength, this,std::placeholders::_1));

		for(OpenDriveMath::Box box : boxes)
		{
			TreeData treeData((*iterator).second,this,box);

			//treeData.geometry = (*iterator).second;
			//treeData.range = box;
			//treeData.road = this;

			roadNodes.push_back(treeData);
			//TreeData(std::shared_ptr<Geometry> g,std::shared_ptr<Rd> r,OpenDriveMath::Box ra);
		}
	}
	return roadNodes;
}

UserPointData Road::getPosition(double s, double t)
{
	//auto a = *(data.offsets.equal_range(s)).first;
	//auto o = a.second;
	//double offs = o->offset;
	UserPointData userPointData;
	OpenDriveMath::Point point = data.geometries.equal_range(s).first->second->getPosition(s,t);

	userPointData.x = point.x;
	userPointData.y = point.y;
	userPointData.z = data.elevations.equal_range(s).first->second->getElevation(s);

	if(!data.lateralProfiles.empty())
	{
		userPointData.z += data.lateralProfiles.equal_range(s).first->second->getElevation(t);
	}

	return userPointData;
}

UserLaneData Road::getLaneData(UserRoadData userRoadData)
{
	return data.lanes.equal_range(userRoadData.sPosition).first->second->getLaneDataFromRoadData(userRoadData);
}

std::pair<double,double> Road::getLaneLength(double s)
{
	auto lower = data.lanes.lower_bound(s);

	if(data.lanes.size() == 0)
	{
		return std::pair<double,double>(0.0,0.0);
	}
	else if(lower == data.lanes.begin())
	{
		return lower->second->getLength(s);
	}
	else if (lower==data.lanes.end())
	{
		//std::prev(data.lanes.end());
		//auto a = (--data.lanes.end());
		return std::prev(data.lanes.end())->second->getLength(s);
	}
}
