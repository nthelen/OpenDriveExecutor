/*
 * Speed.h
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#ifndef SPEED_H_
#define SPEED_H_

#include <string>

#include <vector>
#include <memory>
#include "../Roads/Lane.h"
#include "../XML/pugixml.hpp"
#include <utility>

struct SpeedData
{
	double speed;
	std::string units;
};

class Speed
{

public:

	Speed(pugi::xml_node node);
	virtual ~Speed();
	const SpeedData& getData() const {return data;}
	Speed(){};
protected:
	SpeedData data;

};

#endif /* SPEED_H_ */
