/*
 * Signal.h
 *
 *  Created on: Feb 12, 2016
 *      Author: thelenn1
 */

#ifndef SIGNAL_H_
#define SIGNAL_H_

#include <vector>
#include <memory>
#include "../Roads/Lane.h"
#include "../XML/pugixml.hpp"
#include <utility>

enum Dynamic { Yes=0, No=1};
enum Orientation { Plus=0, Minus=1, Both=2};

struct SignalValidity
{
	SignalValidity(int af, int at) : fromLane(af), toLane(at) {};
	int fromLane;
	int toLane;
};

struct SignalDependency
{
	SignalDependency(std::string aid, std::string atype) : id(aid), type(atype) {};
	std::string id;
	std::string type;
};

struct SignalData
{
	std::vector< SignalValidity > signalValidities;
	std::vector< SignalDependency > signalDependencies;

	double s;
	double t;
	std::string id;
	std::string name;
	Dynamic dynamic;
	Orientation orientation;
	double zOffset;
	std::string country;
	std::string type;
	std::string subtype;
	double value;
	std::string unit;
	double height;
	double width;
	std::string text;
	double hOffset;
	double pitch;
	double roll;

};

struct RoadSpecificSignalData
{
	std::vector< SignalValidity > signalValidities;
	std::vector< SignalDependency > signalDependencies;

	double s;
	double t;
	std::string id;
	std::string name;
};

class Signal {
public:
	Signal(pugi::xml_node node);
	virtual ~Signal();
	const SignalData& getData() const{ return data; }
protected:
	SignalData data;

	SignalValidity handleSignalValidity(pugi::xml_node node);
	SignalDependency handleSignalDependency(pugi::xml_node node);
};

#endif /* SIGNAL_H_ */
