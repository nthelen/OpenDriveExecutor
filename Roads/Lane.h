#ifndef LANE_H_
#define LANE_H_


#include <vector>
#include <map>
#include <memory>
#include "../XML/pugixml.hpp"
#include <utility>

enum LanePosition {Center = 0, Left = 1, Right = 2};
enum LaneType {Driving = 0, Sidewalk = 1, None = 2, Border = 3};

struct LaneOffset
{
	LaneOffset(double,double,double,double,double);// : offset(aoffset),a(aa),b(ab),c(ac),d(ad);
	double offset, a, b, c, d;

};

struct LaneData
{
	int id;
	LaneType type;
	LanePosition position;
	double level;
	std::string predecessor;
	std::string successor;
	double hoffset, hInner, hOuter;
	bool widthType;
	std::map<double,std::shared_ptr<LaneOffset> > offsets;
};

class Lane
{
public:
	double getLength(double s);
	const LaneData& getData() const {return data;};
	Lane(pugi::xml_node);
	Lane(){};
protected:
	LaneData data;
	LaneOffset* HandleOffset(pugi::xml_node node);
	bool HandleLinks(pugi::xml_node node);
	bool HandleSection(pugi::xml_node node);


private:


};

#endif
