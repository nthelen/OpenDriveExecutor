/*
 * Lane.h
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#ifndef LANESD_H_
#define LANESD_H_

#include <vector>
#include <memory>
#include "../XML/pugixml.hpp"
#include "../UserData/UserData.h"
#include <utility>
#include <boost/lexical_cast.hpp>
#include "Lane.h"

struct LanesData
{
	double offset;
	double a;
	double b;
	double c;
	double d;
	double s;
	bool singleSide;

	std::vector< std::shared_ptr<Lane> > RightLanes;
	std::vector< std::shared_ptr<Lane> > CenterLanes;
	std::vector< std::shared_ptr<Lane> > LeftLanes;
};

class Lanes
{
	public:
		Lanes(pugi::xml_node node);
		Lanes(){};
		std::pair<double,double> getLength(double as);
		//UserLaneData getLaneDataFromRoadData(UserRoadData road);
		UserRoadData getRoadDataFromLaneData(UserLaneData road) const;
		UserLaneData getLaneDataFromRoadData(UserRoadData road);
	protected:
		bool HandleOffset(pugi::xml_node node);
		bool HandleSection(pugi::xml_node node);
		bool AddLanes(pugi::xml_node node, LanePosition position);
		double getOffset(double as);
		const LanesData& getData(){return data;}
		LanesData data;
	private:
};

#endif /* LANES_H_ */

