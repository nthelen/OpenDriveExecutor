/*
 * Lane.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#include <memory>
#include <boost/lexical_cast.hpp>
#include "Lane.h"

LaneOffset::LaneOffset(double aoffset,double aa,double ab,double ac,double ad) : offset(aoffset),a(aa),b(ab),c(ac),d(ad)
{}

Lane::Lane(pugi::xml_node node)
{
	for (pugi::xml_node node : node.children())
	{
		if (std::string(node.name()) == std::string("link"))
		{
			HandleLinks(node);
		}
		if (std::string(node.name()) == std::string("width"))
		{
			std::shared_ptr<LaneOffset> laneOffset = std::shared_ptr<LaneOffset>(HandleOffset(node));
			data.offsets[laneOffset->offset] = laneOffset;
			data.widthType = true;
		}
		if (std::string(node.name()) == std::string("border"))
		{
			HandleOffset(node);
			data.widthType = false;
		}
	}
}

bool Lane::HandleLinks(pugi::xml_node node)
{
	for (pugi::xml_node node : node.children())
	{
		if (std::string(node.name()) == std::string("predecessor"))
		{
			data.predecessor= node.attribute("id").value();
		}
		if (std::string(node.name()) == std::string("successor"))
		{
			data.successor= node.attribute("id").value();
		}
	}
}

double Lane::getLength(double s)
{
	auto lower = data.offsets.lower_bound(s);
	double offset = 0;

	if(lower != data.offsets.end())
	{
		offset = lower->second->offset - s;
	}
	else if (lower == data.offsets.end())
	{
		lower = std::prev(data.offsets.end());
		offset = lower->second->offset - s;
	}
	return lower->second->a+lower->second->b*offset+lower->second->c*offset*offset+lower->second->d*offset*offset*offset;
}

LaneOffset* Lane::HandleOffset(pugi::xml_node node)
{
	double offset,a,b,c,d;

	for (pugi::xml_attribute attr : node.attributes())
	{
		if (std::string(attr.name()) == std::string("sOffset"))
		{
			offset = boost::lexical_cast<double>(attr.value());
		}
		if (std::string(attr.name()) == std::string("a"))
		{
			a = boost::lexical_cast<double>(attr.value());
		}
		if (std::string(attr.name()) == std::string("b"))
		{
			b = boost::lexical_cast<double>(attr.value());
		}
		if (std::string(attr.name()) == std::string("c"))
		{
			c = boost::lexical_cast<double>(attr.value());
		}
		if (std::string(attr.name()) == std::string("d"))
		{
			d = boost::lexical_cast<double>(attr.value());
		}
	}
	  return new LaneOffset(offset,a,b,c,d);

}


