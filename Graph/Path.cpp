/*
 * Path.cpp
 *
 *  Created on: Feb 18, 2016
 *      Author: thelenn1
 */

#include "Path.h"
#include <boost/lexical_cast.hpp>
#include <memory>
#include <functional>

OpenDrivePath::OpenDrivePath()
{


}

double OpenDrivePath::getPathDistance(std::string id, double s)
{
	double distance = 0;
	std::string previousId = "";
	for(PathNode node : nodes)
	{
		if(node.id == id)
		{
			return distance+abs(boost::lexical_cast<int>(node.position-s));
		}
		else if (previousId == node.id)
		{
			distance+=node.position;
		}
	}
	throw std::runtime_error("Not in path");
}

void OpenDrivePath::addNode(PathNode node)
{
	nodes.push_back(node);
	return;
}


bool OpenDrivePath::RoadExists(std::string road)
{
	for(PathNode node : nodes)
	{
		if(node.id == road)
			return true;
	}
		return false;
}

std::vector<RoadNode*>  OpenDrivePath::AStar(std::function< std::vector< std::shared_ptr<RoadNode> > (std::string) > roads,
		RoadNode* start,
		RoadNode* goal)
{
	RoadNode* current;
    // The set of nodes already evaluated.
	std::set<RoadNode*> closedSet;
    //closedSet := {}
    // The set of currently discovered nodes still to be evaluated.
    // Initially, only the start node is known.
    std::set<RoadNode*> openSet;

    openSet.insert(start);

    //openSet := {start}
    // For each node, which node it can most efficiently be reached from.
    // If a node can be reached from many nodes, cameFrom will eventually contain the
    // most efficient previous step.
    std::map<RoadNode*,RoadNode*> cameFrom;

    // For each node, the cost of getting from the start node to that node.
    std::map<RoadNode*,double> gScore;
    // The cost of going from start to start is zero.
    gScore[start] = 0.0;
    // For each node, the total cost of getting from the start node to the goal
    // by passing by that node. That value is partly known, partly heuristic.
	std::map<RoadNode*,double> fScore;
    // For the first node, that value is completely heuristic.

	//arbitrary number for now
    fScore[start] = start->location.distance(goal->location)*start->location.distance(goal->location);


    while(!openSet.empty())
    {
    	RoadNode* current = getLowest(fScore);
        //current := the node in openSet having the lowest fScore[] value
        if (current->roadId == goal->roadId)
        {
        	cameFrom[goal] = current;
            return reconstructPath(cameFrom, current);
        }

        openSet.erase(current);
        closedSet.insert(current);
        for (RoadNode* neighbor : current->connectingNodes)
		{
            if (closedSet.find(neighbor) != closedSet.end())
                continue;
            // Ignore the neighbor which is already evaluated.
            // The distance from start to a neighbor
            double tentativeGScore = gScore[current] + current->location.distance(neighbor->location);
            if (openSet.find(neighbor) != openSet.end())	// Discover a new node
                openSet.insert(neighbor);
            else if (tentativeGScore >= gScore[neighbor])
                continue;
            // This is not a better path.

            // This path is the best until now. Record it!
            cameFrom[neighbor] = current;
            gScore[neighbor] = tentativeGScore;
            fScore[neighbor] = gScore[neighbor] + neighbor->location.distance(goal->location)*neighbor->location.distance(goal->location);
		}
    }
    throw std::runtime_error("Could not find path");
}

RoadNode* OpenDrivePath::getLowest(const std::map<RoadNode*,double>& scoreMap)
{
	double min = 99999999;
	RoadNode* returnValue;

	for(std::pair<RoadNode*,double> node : scoreMap)
	{
		if(node.second < min)
		{
			min=node.second;
			returnValue=node.first;
		}
	}
	return returnValue;
}

std::vector<RoadNode*> OpenDrivePath::reconstructPath(std::map<RoadNode*,RoadNode*> cameFrom, RoadNode* current)
{
	std::vector<RoadNode*> totalPath;

	totalPath.push_back(current);

    while (cameFrom.find(current) != cameFrom.end())
    {
        current = cameFrom[current];
        totalPath.push_back(current);
    }

    return totalPath;
}

/*
OpenDrivePath::PathNode OpenDrivePath::dijkstra(std::function< std::vector< std::shared_ptr<RoadNode> > (std::string) > roads,
		RoadNode* start,
		RoadNode* finish)
{

    std::map<const RoadNode&,double> minDistance;
	std::vector< std::pair< std::vector<RoadNode>,const RoadNode&> > activeVertices;
    minDistance[ &start ] = 0;

    std::pair< std::vector<RoadNode>,const RoadNode&> currentLocation= start;
    std::vector<RoadNode> currentRoute;

	if (currentLocation == finish)
	{
		return minDistance[currentLocation];
	}

	currentRoute.push_back(RoadNode(start));
	activeVertices.push_back(std::pair< std::vector<RoadNode>,const RoadNode&>(currentRoute,start));

    while(activeVertices.size() > 0)
    {
    	currentLocation = activeVertices[0].second;
    	activeVertices.erase( activeVertices.begin() );

    	const std::vector<const RoadNode& >& roads = roads(currentLocation.second.roadId);

		for (const RoadNode& edge : roads)
		{
			if ((minDistance.find(currentLocation.second) == minDistance.end())
					|| minDistance[edge] > minDistance[currentLocation.second] + edge.s)
			{
				activeVertices.erase(activeVertices.begin());
				minDistance[edge] = minDistance[currentLocation.second] + edge.s;
			}
			else
			{

			}
		}
    }
    throw "No valid Path";
}
*/

OpenDrivePath::PathNode* OpenDrivePath::getShortestDistance(const std::vector<PathNode>& paths)
{
	OpenDrivePath::PathNode* returnNode = NULL;
	double minDistance =99999999;
	for(PathNode path : paths)
	{
		if(path.position < minDistance)
		{
			returnNode = &path;
			minDistance = path.position;
		}
	}
	if (returnNode == NULL)
		throw std::runtime_error("Path node not found");

	return returnNode;
}

bool OpenDrivePath::RoadExists(const std::vector<PathNode>& path, std::string roadId)
{
	for(OpenDrivePath::PathNode node : path)
	{
		if(node.id == roadId)
			return true;

	}
	return false;
}

std::vector<OpenDrivePath::PathNode> OpenDrivePath::travelNode(const std::vector<OpenDrivePath::PathNode>& path
		, const std::vector<RoadNode>& nodes)
{
	std::vector<OpenDrivePath::PathNode> returnNodes;
	for(const RoadNode& node : nodes )
	{
		std::vector<OpenDrivePath::PathNode> newPath(path);
		if(!RoadExists(newPath, node.roadId))
		{

			//newPath.push_back(PathNode(node.));
			//newPath.push_back(PathNode(node.roadId,node.s,;
		}
	}
	return returnNodes;
}

