/*
 * Path.h
 *
 *  Created on: Feb 18, 2016
 *      Author: thelenn1
 */

#ifndef GRAPH_PATH_H_
#define GRAPH_PATH_H_

#include <set>
#include <map>
#include <vector>
#include <string>
#include "RoadNode.h"


class OpenDrivePath
{
	class PathNode
	{
		public:
		PathNode(std::string aid,double aposition, double alength) : position(aposition), id(aid), length(0){}
		double position;
		std::string id;
		double length;
		private:
		PathNode();

	};
protected:
	std::vector<PathNode> nodes;

public:
	bool addVertex(std::string id, double s);
	std::vector<OpenDrivePath::PathNode> travelNode(const std::vector<OpenDrivePath::PathNode>& path
			, std::vector<const RoadNode&> nodes);

	//PathNode dijkstra(std::function< const std::vector<RoadNode>& (std::string) const> roads,
	//		const RoadNode& start,
	//		const RoadNode& finish);

	std::vector<RoadNode*> reconstructPath(std::map<RoadNode*,RoadNode*> cameFrom, RoadNode* current);
	RoadNode* getLowest(const std::map<RoadNode*,double>& scoreMap);
	std::vector<RoadNode*>  AStar(std::function< std::vector< std::shared_ptr<RoadNode> > (std::string) > roads,
			RoadNode* start,
			RoadNode* goal);

	PathNode* getShortestDistance(const std::vector<PathNode>& paths);
	//PathNode dijkstra(const std::map<std::string,Road> &roads,const RoadNode& start,const RoadNode& finish);
	void addNode(PathNode node);
	double getPathDistance(std::string id, double s);
	OpenDrivePath();
	bool RoadExists(std::string road);
	bool RoadExists(const std::vector<PathNode>& path, std::string roadId);

	std::vector<OpenDrivePath::PathNode> travelNode(const std::vector<OpenDrivePath::PathNode>& path
			, const std::vector<RoadNode>& nodes);

	struct Pathfinder
	{
		std::string roadName;
		std::vector<std::string> path;
	};


};

#endif /* GRAPH_PATH_H_ */
