#ifndef RoadNode_PATH_H_
#define RoadNode_PATH_H_

#include "../Math/Point.h"
#include <string>

class RoadNode
{
	public:
		RoadNode(std::string aroadId, double as) : roadId(aroadId), s(as),location(0,0){};
		std::string roadId;
		double s;
		std::vector<RoadNode*> connectingNodes;
		OpenDriveMath::Point location;
		RoadNode() : location(0,0){};

	private:

};

#endif
