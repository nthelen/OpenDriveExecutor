/*
 * Control.h
 *
 *  Created on: Feb 6, 2016
 *      Author: thelenn1
 */

#ifndef CONTROL_H_
#define CONTROL_H_

#include <vector>
#include <memory>
#include "XML/pugixml.hpp"
#include <utility>

struct ControlData
{
	std::string signalId;
	std::string type;
};

class Control {
public:
	Control(pugi::xml_node node);
	Control();
	virtual ~Control();
	const ControlData& getControlData() const { return data;}


private:
	ControlData data;

};

#endif /* CONTROL_H_ */
