/*
 * TreeData.h
 *
 *  Created on: Jun 8, 2016
 *      Author: thelenn1
 */

#ifndef TREES_TREEDATA_H_
#define TREES_TREEDATA_H_

#include "../Math/Box.h"
#include "../Geometry/Geometry.h"

class Road;

class TreeData
{
	public:
		//TreeData(){ road = NULL;};
		TreeData(std::shared_ptr<Geometry> g,Road* r,OpenDriveMath::Box ra);
		std::shared_ptr<Geometry> geometry;
		Road* road;
		OpenDriveMath::Box range;
};

#endif /* TREES_TREEDATA_H_ */
