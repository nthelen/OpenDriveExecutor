/*
 * TreeData.cpp
 *
 *  Created on: Jun 8, 2016
 *      Author: thelenn1
 */

#include "TreeData.h"

TreeData::TreeData(std::shared_ptr<Geometry> g,Road* r,OpenDriveMath::Box ra) : geometry(g),
		road(r),range(ra)
{}
