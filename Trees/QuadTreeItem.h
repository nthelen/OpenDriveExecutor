/*
 * QuadTreeItem.h
 *
 *  Created on: Feb 9, 2016
 *      Author: thelenn1
 */

#ifndef TREES_QUADTREEITEM_H_
#define TREES_QUADTREEITEM_H_

#include <memory>

#include "../Math/Box.h"
#include "../Roads/Road.h"
#include "../Geometry/Geometry.h"
#include "TreeData.h"



template<class T>
class QuadTreeItem {
public:
	QuadTreeItem(T* t);
	std::shared_ptr<T> item;
	//OpenDriveMath::Box box;
private:
	QuadTreeItem();
};

#endif /* TREES_QUADTREEITEM_H_ */
