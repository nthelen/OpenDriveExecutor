/*
 * Node.cpp
 *
 *  Created on: Feb 9, 2016
 *      Author: thelenn1
 */

#include "QuadNode.h"
#include "../Math/Box.h"
#include <iostream>

template<class T>
QuadNode<T>::QuadNode(OpenDriveMath::Box abox, int amax_items) : max_items(amax_items) , box(abox), nw(0), ne(0),sw(0),se(0)
{
}

template<class T>
bool QuadNode<T>::insert(T item)
{
	if(box.intersects(item.range))
	{
		std::cout << "Push back item" << item.range.getPoint(0).x << " " << item.range.getPoint(0).y
				<< item.range.getPoint(1).x << " " << item.range.getPoint(1).y << " " << item.road->getData().id << "\n";
		items.push_back(item);

		if(items.size() >= max_items)
		{
			breakNode();
		}
		return true;
	}
	return false;

}

template<class T>
const std::vector< T >& QuadNode<T>::get(OpenDriveMath::Point point)
{
	std::cout << "Get Item from node:" << point.x << " " << point.y << " " << box.getPoint(0).x <<
			" " << box.getPoint(0).y << " " << box.getPoint(1).x <<
			" " << box.getPoint(1).y << "\n";
	if(box.contains(point))
	{
		std::cout << "Get Item from node 2\n";
		if(nw)
		{
			std::cout << "nw exists\n";
			try
			{
				return nw->get(point);
			}
			catch(...)
			{
				//Don't care
			}
			try
			{
				return ne->get(point);
			}
			catch(...)
			{
				//Don't care
			}
			try
			{
				return sw->get(point);
			}
			catch(...)
			{
				//Don't care
			}
			try
			{
				return se->get(point);
			}
			catch(...)
			{
				//Don't care
			}
		}
		else
		{
			std::cout << "Returning" << items.size() << "\n";
			return items;
		}
	}
	else
	{
		throw std::runtime_error("Could not find item");
	}

}

template<class T>
void QuadNode<T>::breakNode()
{
	//std::cout << "Breaking node:" << box.getCenter().x << " " << box.getCenter().y << " " << box.getPoint(0).x <<
	//		" " << box.getPoint(0).y << " " << box.getPoint(1).x << " " << box.getPoint(1).y <<"\n";
	nw = std::make_shared< QuadNode<T> >(OpenDriveMath::Box(box.getMin(),box.getCenter()),max_items);
	ne = std::make_shared< QuadNode<T> >(OpenDriveMath::Box(OpenDriveMath::Point(box.getMax().x,box.getMin().y),
			box.getCenter()),max_items);
	sw = std::make_shared< QuadNode<T> >(OpenDriveMath::Box(OpenDriveMath::Point(box.getMin().x,box.getMax().y),
			box.getCenter()),max_items);
	se = std::make_shared< QuadNode<T> >(OpenDriveMath::Box(box.getMax(),box.getCenter()),max_items);

	for(auto item : items)
	{
		//std::cout << "Insert:" <<
		nw->insert(item);
		ne->insert(item);
		sw->insert(item);
		se->insert(item);;
	}

	items.clear();
}

template class QuadNode<TreeData>;
