/*
 * Node.h
 *
 *  Created on: Feb 9, 2016
 *      Author: thelenn1
 */

#ifndef TREES_QUADNODE_H_
#define TREES_QUADNODE_H_

#include <set>
#include <memory>
#include <vector>
#include "QuadTreeItem.h"

template<class T>
class QuadNode
{
public:
	QuadNode(OpenDriveMath::Box abox, int amax_items);

	std::shared_ptr<QuadNode<T> > nw;
	std::shared_ptr<QuadNode<T> > ne;
	std::shared_ptr<QuadNode<T> > sw;
	std::shared_ptr<QuadNode<T> > se;


	const std::vector< T>& get(OpenDriveMath::Point point);

	//template<class T>
	bool insert(T item);

	//template<class T>
	std::vector< T > items;

private:
	int max_items;
	void breakNode();
	OpenDriveMath::Box box;
};

#endif /* TREES_QUADNODE_H_ */
