/*
 * QuadTree.cpp
 *
 *  Created on: Feb 9, 2016
 *      Author: thelenn1
 */
#include <iostream>
#include "QuadTree.h"
#include "QuadTreeItem.h"
#include "../Math/Box.h"
#include "QuadNode.h"
#include "../MainNode.h"

template<class T>
QuadTree<T>::QuadTree(OpenDriveMath::Box box,int max_items)
{
	root = std::make_shared< QuadNode<TreeData> >(box,max_items);
}


template<class T>
std::vector<T >QuadTree<T>::getItems(OpenDriveMath::Point p)
{
	return root->get(p);
}
template<class T>
std::vector<T >QuadTree<T>::getItems(OpenDriveMath::Box box)
{
	std::vector<T > returnValue;
	for(auto item : root->get(box.getPoint(0)))
	{
		returnValue.push_back(item);
	}
	for(auto item : root->get(box.getPoint(1)))
	{
		returnValue.push_back(item);
	}
	for(auto item : root->get(box.getMax()))
	{
		returnValue.push_back(item);
	}
	for(auto item : root->get(box.getMin()))
	{
		returnValue.push_back(item);
	}

	return returnValue;
}

template<class T>
void QuadTree<T>::insert(T* item)
{
	//std::cout << "Insert Item\n";
	root->insert(*item);
};

template class QuadTree<TreeData>;
