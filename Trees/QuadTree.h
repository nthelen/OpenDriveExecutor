/*
 * QuadTree.h
 *
 *  Created on: Feb 9, 2016
 *      Author: thelenn1
 */

#ifndef TREES_QUADTREE_H_
#define TREES_QUADTREE_H_

#include "QuadNode.h"
#include "QuadTreeItem.h"

template<class T>
class QuadTree
{
public:
	QuadTree(OpenDriveMath::Box box,int max_items);

	std::shared_ptr< QuadNode<T> > root;

	void insert(T* item);

	std::vector< T > getItems(OpenDriveMath::Point);
	std::vector< T > getItems(OpenDriveMath::Box);
};

#endif /* TREES_QUADTREE_H_ */
