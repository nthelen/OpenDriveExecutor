/*
 * Point.cpp
 *
 *  Created on: Jan 28, 2016
 *      Author: thelenn1
 */

#include "Point.h"
#include "Box.h"
#include <math.h>;

namespace OpenDriveMath
{
	Point::Point(double ax, double ay) : x(ax), y(ay)
	{
	}

	double Point::distance(const Point& offset)
	{
		Point diffPoint = diff(offset);
		return sqrt(diffPoint.x*diffPoint.x + diffPoint.y*diffPoint.y);
	}

	Point Point::diff(const Point& offset) const
	{
		return Point(x-offset.x,y-offset.y);
	}

	double Point::getHeading(Point offset)
	{
		return atan2(y - offset.y, x - offset.x);
	}

	OpenDriveMath::Point Point::moveOnHeading(double heading, double s)
	{
		return Point(x+cos(heading)*s,y+sin(heading)*s);
	}

	OpenDriveMath::Point Point::Transform(Point offset)
	{
		return Point(x+offset.x,y+offset.y);
	}

	OpenDriveMath::Point Point::Rotate(double heading)
	{
		return OpenDriveMath::Point(cos(heading) * x - sin(heading) * y ,sin(heading) * x + cos(heading) * y);
	}

	bool Point::isInside(const Box& box)
	{
		bool result = false;
		if(x < box.getPoint(0).x && x>box.getPoint(1).x )
		{
			if(y < box.getPoint(0).y && y >box.getPoint(1).y )
				result=true;
			else result = false;
		}
		if(x > box.getPoint(0).x && x<box.getPoint(1).x )
		{
			if(y > box.getPoint(0).y && y < box.getPoint(1).y )
				result=true;
			else result = false;
		}
		return result;
	}

}
