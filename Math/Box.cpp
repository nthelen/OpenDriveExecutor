/*
 * Box.cpp
 *
 *  Created on: Jan 28, 2016
 *      Author: thelenn1
 */

#include <Math.h>
#include <stdexcept>
#include "Box.h"
#include "Point.h"

namespace OpenDriveMath
{
	Box::Box(Point* p1,Point* p2) : point1(p1->x,p1->y), point2(p2->x,p2->y)
	{

	}

	Box::Box(const Point& p1,const Point& p2) : point1(p1.x,p1.y), point2(p2.x,p2.y)
	{
	}

	Box::~Box()
	{

	}

	bool Box::contains(OpenDriveMath::Point point) const
	{
		Point p1 = getMin();
		Point p2 = getMax();

		if(p1.x < point.x-0.00001 && p2.x > point.x+0.00001 && p1.y < point.y-0.00001 && p2.y > point.y + 0.00001)
			return true;
		return false;
	}

	Point Box::getMin() const
	{
		double x,y;

		(point1.x < point2.x) ? x = point1.x : x = point2.x;
		(point1.y < point2.y) ? y = point1.y : y = point2.y;
		return OpenDriveMath::Point(x,y);
	}

	Point Box::getMax() const
	{
		double x,y;

		(point1.x > point2.x) ? x = point1.x : x = point2.x;
		(point1.y > point2.y) ? y = point1.y : y = point2.y;
		return OpenDriveMath::Point(x,y);
	}

	Point Box::getCenter() const
	{
		return OpenDriveMath::Point((point1.x+point2.x)/2.0,(point1.y+point2.y)/2.0);
	}

	Point Box::getPoint(int i) const
	{
		if(i==0)
			return point1;
		if(i==1)
			return point2;
		else throw std::runtime_error("Out of Range");
	}

	bool Box::intersects(OpenDriveMath::Box box) const
	{
		OpenDriveMath::Point p1 = point1;
		OpenDriveMath::Point p2 = point2;
		OpenDriveMath::Point p3 = getMax();
		OpenDriveMath::Point p4 = getMin();

		OpenDriveMath::Point bp1 = box.getPoint(0);
		OpenDriveMath::Point bp2 = box.getPoint(1);
		OpenDriveMath::Point bp3 = box.getMax();
		OpenDriveMath::Point bp4 = box.getMin();

		if(contains(bp1) || contains(bp2) || contains(bp3) || contains(bp4))
			return true;
		//if(box.contains(p1) || box.contains(p2) || box.contains(p3) || box.contains(p4))
		//	return true;
		return false;
	}

	bool Box::isInside(OpenDriveMath::Box box) const
	{
		OpenDriveMath::Point p1 = point1;
		OpenDriveMath::Point p2 = point2;
		OpenDriveMath::Point p3 = OpenDriveMath::Point(point1.x,point2.y);
		OpenDriveMath::Point p4 = OpenDriveMath::Point(point2.x,point1.y);;

		if(box.contains(p1) && box.contains(p2) && box.contains(p3) && box.contains(p4))
			return true;
		return false;
	}

}
