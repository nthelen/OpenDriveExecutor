/*
 * LinearLine.h
 *
 *  Created on: Feb 29, 2016
 *      Author: thelenn1
 */

#ifndef MATH_LINEARLINE_H_
#define MATH_LINEARLINE_H_

#include "Point.h"

namespace OpenDriveMath
{

class LinearLine
{
	public:
		LinearLine(const Point&,const Point&);
		LinearLine(double y, double slope);
		Point getIntersection(const LinearLine&);

		double b;
		double slope;

	protected:
		double calculateB(Point p1);
		double calculateSlope(const Point& p1,const Point& p2);
};

}
#endif /* MATH_LINEARLINE_H_ */
