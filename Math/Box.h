/*
 * Box.h
 *
 *  Created on: Jan 28, 2016
 *      Author: thelenn1
 */


#ifndef BOX_H_
#define BOX_H_

#include "Point.h"

namespace OpenDriveMath
{
	class Box
	{
		public:
			Box();
			Box(Point* p1,Point* p2);
			Box(const Point& p1,const Point& p2);
			virtual ~Box();
			bool contains(Point point) const;
			Point getMin() const;
			Point getMax() const;
			Point  getPoint(int i) const;
			bool intersects(Box box) const;
			bool isInside(Box box) const;
			Point getCenter() const;
		protected:
			Point point1, point2;
		private:

	};

}
#endif /* BOX_H_ */
