/*
 * LinearLine.cpp
 *
 *  Created on: Feb 29, 2016
 *      Author: thelenn1
 */

#include "LinearLine.h"
#include "Point.h"

namespace OpenDriveMath
{

	LinearLine::LinearLine(const Point& p1,const Point& p2)
	{
		b = calculateB(p1);
		slope = calculateSlope(p1,p2);
	}
	LinearLine::LinearLine(double ay,double aslope) : b(ay) , slope(aslope)
	{}

	double LinearLine::calculateB(Point p1)
	{
		//y=mx+b
		b = p1.y - slope*p1.x;
		return b;
	}

	double LinearLine::calculateSlope(const Point& p1,const Point& p2)
	{
		Point diffPoint = p2.diff(p1);
		return diffPoint.y/diffPoint.x;
	}

	Point LinearLine::getIntersection(const LinearLine& line)
	{
		double tempSlope = slope-line.slope;
		double tempB = line.b - b;

		double x = tempB/tempSlope;
		double y = slope*x+b;

		return Point(x,y);
	}

}
