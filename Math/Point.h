/*
 * Point.h
 *
 *  Created on: Jan 28, 2016
 *      Author: thelenn1
 */

#ifndef POINT_H_
#define POINT_H_



namespace OpenDriveMath
{

class Box;

class Point
{
	public:
		Point(double ax, double ay);
		Point diff(const Point& offset) const;
		Point moveOnHeading(double heading, double s);
		bool isInside(const Box& box);
		double getHeading(Point offset);
		Point Transform(Point offset);
		Point Rotate(double heading);
		double distance(const Point& offset);

		double x;
		double y;

		Point() = delete;
	private:

	};

}
#endif /* POINT_H_ */
