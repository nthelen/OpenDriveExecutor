/*
 * UserData.h
 *
 *  Created on: Feb 26, 2016
 *      Author: thelenn1
 */

#ifndef USERDATA_USERDATA_H_
#define USERDATA_USERDATA_H_

struct UserLaneData;
struct UserRoadData;
struct UserPointData;
struct UserRouteData;
struct Path;

#include <string>
//#include "../Roads/Road.h"
#include <vector>
#include <memory>
#include "../Roads/Lane.h"
#include "../XML/pugixml.hpp"
#include "../Graph/Path.h"
#include <utility>

/*UserRoadData UserLaneData::toRoad()
UserPointData UserLaneData::toPoint();
RouteData UserLaneData::toRoute(Path path);
UserPointData& UserRoadData::toPoint();
RouteData& UserRoadData::toRoute(Path path);
UserLaneData& UserRoadData::toLane();
UserRoadData UserPointData::toRoad()
RouteData UserPointData::toRoute(Path path)
UserLaneData UserPointData::toLane()
UserRoadData UserRouteData::toRoad()
UserPointData UserRouteData::toPoint()
UserLaneData UserRouteData::toLane()
*/

struct UserLaneData
{
	std::string roadId;
	int laneNumber;
	double laneOffset;
	double roadOffset;
};

struct UserRoadData
{
	std::string road;
	double sPosition;
	double roadOffset;
};

struct UserPointData
{
	double x;
	double y;
	double z;
};

struct UserRouteData
{
	double totalPosition;
	double roadOffset;
	std::string road;
	double roadPosition;
	double sPosition;
};

#endif /* USERDATA_USERDATA_H_ */
