#ifndef CONSTANTS_FILE
#define CONSTANTS_FILE

namespace Constants
{
	static double BoxIteration = 10;
	static int MaxTreeItems = 5;
	static double DEGREES_TO_RADIANS = 3.14/180.0;
}

#endif
