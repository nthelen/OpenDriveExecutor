/*
 * OpenDriveMain.h
 *
 *  Created on: Feb 4, 2016
 *      Author: thelenn1
 */

#ifndef OPENDRIVEMAIN_H_
#define OPENDRIVEMAIN_H_

#include <string>


struct VersionInfo
{
	int revMajor;
	int revMinor;
	std::string name;
	std::string version;
	std::string date;
	double north ;
	double south;
	double east;
	double west;
	std::string vendor;
};

class OpenDriveMain
{
protected:

		VersionInfo versionInfo;
	OpenDriveMain();
	virtual ~OpenDriveMain();
};

#endif /* OPENDRIVEMAIN_H_ */
