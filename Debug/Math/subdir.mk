################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Math/Box.cpp \
../Math/LinearLine.cpp \
../Math/Point.cpp 

OBJS += \
./Math/Box.o \
./Math/LinearLine.o \
./Math/Point.o 

CPP_DEPS += \
./Math/Box.d \
./Math/LinearLine.d \
./Math/Point.d 


# Each subdirectory must supply rules for building sources it contributes
Math/%.o: ../Math/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


