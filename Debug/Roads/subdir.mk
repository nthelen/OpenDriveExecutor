################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Roads/Lane.cpp \
../Roads/LaneSection.cpp \
../Roads/Lanes.cpp \
../Roads/Link.cpp \
../Roads/Road.cpp \
../Roads/Signal.cpp \
../Roads/Speed.cpp 

OBJS += \
./Roads/Lane.o \
./Roads/LaneSection.o \
./Roads/Lanes.o \
./Roads/Link.o \
./Roads/Road.o \
./Roads/Signal.o \
./Roads/Speed.o 

CPP_DEPS += \
./Roads/Lane.d \
./Roads/LaneSection.d \
./Roads/Lanes.d \
./Roads/Link.d \
./Roads/Road.d \
./Roads/Signal.d \
./Roads/Speed.d 


# Each subdirectory must supply rules for building sources it contributes
Roads/%.o: ../Roads/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


