################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Trees/QuadNode.cpp \
../Trees/QuadTree.cpp \
../Trees/QuadTreeItem.cpp \
../Trees/TreeData.cpp 

OBJS += \
./Trees/QuadNode.o \
./Trees/QuadTree.o \
./Trees/QuadTreeItem.o \
./Trees/TreeData.o 

CPP_DEPS += \
./Trees/QuadNode.d \
./Trees/QuadTree.d \
./Trees/QuadTreeItem.d \
./Trees/TreeData.d 


# Each subdirectory must supply rules for building sources it contributes
Trees/%.o: ../Trees/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


