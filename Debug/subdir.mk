################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Connection.cpp \
../Control.cpp \
../Controller.cpp \
../Junction.cpp \
../MainNode.cpp \
../Object.cpp \
../OpenDriveMain.cpp \
../Priority.cpp 

OBJS += \
./Connection.o \
./Control.o \
./Controller.o \
./Junction.o \
./MainNode.o \
./Object.o \
./OpenDriveMain.o \
./Priority.o 

CPP_DEPS += \
./Connection.d \
./Control.d \
./Controller.d \
./Junction.d \
./MainNode.d \
./Object.d \
./OpenDriveMain.d \
./Priority.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


