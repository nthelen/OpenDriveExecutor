################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Geometry/Arc.cpp \
../Geometry/Crossfall.cpp \
../Geometry/Elevation.cpp \
../Geometry/Geometry.cpp \
../Geometry/LateralProfile.cpp \
../Geometry/Line.cpp \
../Geometry/ParametricCubic.cpp \
../Geometry/Polynomial.cpp \
../Geometry/Shape.cpp \
../Geometry/Spiral.cpp \
../Geometry/SuperElevation.cpp 

OBJS += \
./Geometry/Arc.o \
./Geometry/Crossfall.o \
./Geometry/Elevation.o \
./Geometry/Geometry.o \
./Geometry/LateralProfile.o \
./Geometry/Line.o \
./Geometry/ParametricCubic.o \
./Geometry/Polynomial.o \
./Geometry/Shape.o \
./Geometry/Spiral.o \
./Geometry/SuperElevation.o 

CPP_DEPS += \
./Geometry/Arc.d \
./Geometry/Crossfall.d \
./Geometry/Elevation.d \
./Geometry/Geometry.d \
./Geometry/LateralProfile.d \
./Geometry/Line.d \
./Geometry/ParametricCubic.d \
./Geometry/Polynomial.d \
./Geometry/Shape.d \
./Geometry/Spiral.d \
./Geometry/SuperElevation.d 


# Each subdirectory must supply rules for building sources it contributes
Geometry/%.o: ../Geometry/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


