/*
 * Priority.h
 *
 *  Created on: Feb 6, 2016
 *      Author: thelenn1
 */

#ifndef PRIORITY_H_
#define PRIORITY_H_

#include <vector>
#include <memory>
#include "XML/pugixml.hpp"
#include <utility>

struct PriorityData
{
	std::string high;
	std::string low;
};

class Priority
{
public:
	Priority(pugi::xml_node node);
	virtual ~Priority();
	PriorityData getPriorityData();
private:
	Priority();
	PriorityData data;
};

#endif /* PRIORITY_H_ */
