/*
 * Object.h
 *
 *  Created on: Feb 22, 2016
 *      Author: thelenn1
 */

#ifndef OBJECT_H_
#define OBJECT_H_

#include <vector>
#include <memory>
#include "XML/pugixml.hpp"
#include <utility>

enum ObjectOrientation { Positive=0, Negative=1, ObjectNone=2};

struct RoadSpecificObjectData
{

	std::string id;
	double  s;
	double  t;
	double  zOffset;
	double  validLength;
	ObjectOrientation orientation;
};

struct ObjectData
{
	std::string type;
	std::string name;
	std::string id;
	double  s;
	double  t;
	double  zOffset;
	double  validLength;
	ObjectOrientation orientation;
	double length;
	double width;
	double radius;
	double height;
	double hdg;
	double pitch;
	double roll;
};

class Object
{
protected:
	ObjectData data;

public:
	Object(pugi::xml_node node);
	virtual ~Object();
	const ObjectData& getData() const;
};

#endif /* OBJECT_H_ */
