/*
 * Connection.cpp
 *
 *  Created on: Feb 6, 2016
 *      Author: thelenn1
 */
#include <boost/lexical_cast.hpp>

#include "Connection.h"

Connection::Connection(pugi::xml_node node)
{
	for (pugi::xml_node node : node.children())
	{
		if (node.name() == std::string("laneLink"))
		{
			for (pugi::xml_attribute attr : node.attributes())
			{
				int lfrom, lto;

				data.contactPoint = ContactPoint::START;

				if (std::string(attr.name())== std::string("from"))
					lfrom = boost::lexical_cast<double>(attr.value());
				if (std::string(attr.name()) == std::string("to"))
					lto = boost::lexical_cast<double>(attr.value());

				data.laneLinks.push_back(LaneLink(lfrom, lto));
			}
		}
	}

	for (pugi::xml_attribute attr : node.attributes())
	{
		std::string cp;

		data.contactPoint = ContactPoint::START;

		if (std::string(attr.name()) == std::string("id"))
			data.id = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("incomingRoad"))
			data.incomingRoad = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("connectingRoad"))
			data.connectingRoad = boost::lexical_cast<double>(attr.value());
		if (std::string(attr.name()) == std::string("contactPoint"))
			cp = boost::lexical_cast<double>(attr.value());
		if(cp == std::string("end"))
			data.contactPoint = ContactPoint::END;
	}


}

const ConnectionData& Connection::getConnectionData() const
{
	return data;
}

