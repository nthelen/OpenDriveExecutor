/*
 * Spiral.h
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#ifndef SPIRAL_H_
#define SPIRAL_H_

#include "Geometry.h"

class Spiral: public Geometry
{
protected:
	double curvStart;
	double curvEnd;
	OpenDriveMath::Point getPosition(double os);
	void odrSpiral( double s, double cDot, double *x, double *y, double *t );
public:
	Spiral(pugi::xml_node node);
};

#endif /* SPIRAL_H_ */
