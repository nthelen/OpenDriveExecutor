/*
 * Geometry.cpp
 *
 *  Created on: Jan 27, 2016
 *      Author: thelenn1
 */

#include <math.h>
#include <boost/lexical_cast.hpp>

#include "Geometry.h"
#include "../Math/Point.h"
#include "../Math/Box.h"
#include "../Constants.h"

Geometry::Geometry(pugi::xml_node node)
{
	double x,y;
	// TODO Auto-generated constructor stub
	for (pugi::xml_attribute attr: node.attributes())
	{
		if(std::string(attr.name()) == std::string("s"))
			s = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("x"))
			x = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("y"))
			y = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("hdg"))
			heading= boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("length"))
			length = boost::lexical_cast<double>(attr.value());
	}
		center = std::make_shared<OpenDriveMath::Point>(x,y);

		min= std::make_shared<OpenDriveMath::Point>(-1000000000,-100000000);
		max= std::make_shared<OpenDriveMath::Point>(1000000000,100000000);
		//max(new OpenDriveMath::Point(1000000000,100000000));
}

std::vector<OpenDriveMath::Box> Geometry::makeBoxes(std::function< std::pair<double,double>(double)> laneLength)
{
	std::vector<OpenDriveMath::Box> boxes;
	double s_pos = s;

	while(s_pos < s+length)
	{
		OpenDriveMath::Box temp_box = makeBox(s_pos, laneLength);
		updateMinMax(temp_box);
		s_pos += Constants::BoxIteration;
		boxes.push_back(temp_box);
	}

	return boxes;
}


void Geometry::updateMinMax(OpenDriveMath::Box box)
{
	OpenDriveMath::Point lmin = box.getMin();
	OpenDriveMath::Point lmax = box.getMax();

	if(lmin.x < min->x)
		min->x = lmin.x;
	if(lmin.y < min->y)
		min->y = lmin.y;
	if(lmax.x < max->x)
		max->x = lmax.x;
	if(lmax.y < max->y)
		max->y = lmax.y;

	box = OpenDriveMath::Box(*min,*max);
}
bool Geometry::isInBox(OpenDriveMath::Point point)
{
	for(auto box : boxes)
	{
		if(box.contains(point))
			return true;
	}
	return false;
}

double Geometry::getHeading(double os)
{
	return getPosition(os).getHeading(getPosition(os+Constants::BoxIteration));
}

OpenDriveMath::Box Geometry::makeBox(double os, std::function< std::pair<double,double>(double)> laneLength)
{
	OpenDriveMath::Point pos1 = getPosition(os-s);
	OpenDriveMath::Point pos2 = getPosition((os-s)+Constants::BoxIteration);
	double h1 = getHeading(os-s);

	OpenDriveMath::Point startbottom = OpenDriveMath::Point(pos1.x+cos(h1+90.0*Constants::DEGREES_TO_RADIANS)*laneLength(os).first,
				pos1.y+sin(h1+90.0*Constants::DEGREES_TO_RADIANS)*laneLength(os).first)
				.Transform(*center);
	OpenDriveMath::Point endtop = OpenDriveMath::Point(pos2.x+cos(h1-90.0*Constants::DEGREES_TO_RADIANS)*laneLength(os).second,
				pos2.y+sin(h1-90.0*Constants::DEGREES_TO_RADIANS)*laneLength(os).second)
				.Transform(*center);

	return OpenDriveMath::Box(startbottom, endtop);
}

OpenDriveMath::Point Geometry::getPosition(double os, double t)
{
	return getPosition(os).moveOnHeading(getHeading(s)+90.0*Constants::DEGREES_TO_RADIANS,t).Transform(*center);
}

std::pair<double,double> Geometry::getRoadPosition(OpenDriveMath::Point point, double os,
		std::function< std::pair<double,double>(double)> laneLength)
{
	if(testPosition(point, s, laneLength))
	{
		OpenDriveMath::Point temppoint = getPosition(os-this->s).moveOnHeading(getHeading(os-s),os-s);
		OpenDriveMath::LinearLine line = OpenDriveMath::LinearLine(temppoint,temppoint.moveOnHeading(getHeading(os-s),0.1));
		OpenDriveMath::LinearLine line2 = OpenDriveMath::LinearLine(point,point.moveOnHeading(getHeading(os-s)+90.0,0.1));

		OpenDriveMath::Point temppoint2 = line.getIntersection(line2);

		double offset = temppoint2.distance(temppoint);

		return std::pair<double, double>(s+Constants::BoxIteration/2.0,offset);
	}
	else throw std::runtime_error("Error getting road position");
}

bool Geometry::testPosition(OpenDriveMath::Point point, double s,
		std::function< std::pair<double,double>(double)> laneLength)
{
	if(makeBox(s,laneLength).contains(point))
		return true;
	return false;
}

OpenDriveMath::Point Geometry::getPosition(double os)
{
	return OpenDriveMath::Point(os,0.0);
}
