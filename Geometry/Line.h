/*
 * Line.h
 *
 *  Created on: Feb 4, 2016
 *      Author: thelenn1
 */

#ifndef LINE_H_
#define LINE_H_

#include "Geometry.h"

class Line: public Geometry
{
public:
	Line(pugi::xml_node);
	bool isInBox(OpenDriveMath::Point point);
	virtual ~Line();
	OpenDriveMath::Point getPosition(double os);
};

#endif /* LINE_H_ */
