/*
 * Shape.h
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#ifndef SHAPE_H_
#define SHAPE_H_

#include "LateralProfile.h"

class Shape: public LateralProfile {
public:
	Shape(pugi::xml_node node) ;
	double getElevation(double t);
};

#endif /* SHAPE_H_ */
