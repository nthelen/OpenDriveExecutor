/*
 * SuperElevation.h
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#ifndef SUPERELEVATION_H_
#define SUPERELEVATION_H_

#include "../XML/pugixml.hpp"

#include "LateralProfile.h"

class SuperElevation: public LateralProfile
{
public:
	SuperElevation(pugi::xml_node node);
	double getElevation(double t);

	double a;
	double b;
	double c;
	double d;
};

#endif /* SUPERELEVATION_H_ */
