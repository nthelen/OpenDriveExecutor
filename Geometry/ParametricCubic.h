/*
 * ParametricCubic.h
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#ifndef PARAMETRICCUBIC_H_
#define PARAMETRICCUBIC_H_

#include "Geometry.h"

class ParametricCubic: public Geometry
{
public:
	double aU,bU,cU,dU,aV,bV,cV,dV;
	double range;

	ParametricCubic(pugi::xml_node);
	virtual ~ParametricCubic();
	OpenDriveMath::Point getPosition(double os);
};

#endif /* PARAMETRICCUBIC_H_ */
