/*
 * Crossfall.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */
#include <boost/lexical_cast.hpp>
#include <math.h>

#include "Crossfall.h"
#include "../XML/pugixml.hpp"


Crossfall::Crossfall(pugi::xml_node node) : LateralProfile(node)
{
	for(pugi::xml_attribute attr : node.attributes())
	{
		if(attr.name() == std::string("a"))
			a = boost::lexical_cast<double>(attr.value());
		if(attr.name() == std::string("b"))
			b = boost::lexical_cast<double>(attr.value());
		if(attr.name() == std::string("c"))
			c = boost::lexical_cast<double>(attr.value());
		if(attr.name() == std::string("d"))
			d = boost::lexical_cast<double>(attr.value());
		if(attr.name() == std::string("s"))
			s = boost::lexical_cast<double>(attr.value());
		if(attr.name() == std::string("side"))
		{
			if(attr.value() == std::string("left"))
				side = Side::SideLeft;
			if(attr.value() == std::string("right"))
				s = Side::SideRight;
			if(attr.value() == std::string("both"))
				s = Side::SideBoth;
		}
	}
}

double Crossfall::getElevation(double t)
{
		double dt = abs(t-s);

		return a+b*dt+c*dt*dt+d*dt*dt*dt;
}
