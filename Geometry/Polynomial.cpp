/*
 * Polynomial.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */
#include <boost/lexical_cast.hpp>

#include "Polynomial.h"

Polynomial::Polynomial(pugi::xml_node node) : Geometry(node.parent())
{
	for (pugi::xml_attribute attr: node.attributes())
	{
		if(std::string(attr.name()) == std::string("a"))
			a = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("b"))
			b = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("c"))
			c = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("d"))
			d = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("range"))
			range = boost::lexical_cast<double>(attr.value());
	}

}

OpenDriveMath::Point Polynomial::getPosition(double os)
{
	double offset = os-s;

	return OpenDriveMath::Point(offset,
			a + b*offset + c*offset*offset + d*offset*offset*offset);
}
