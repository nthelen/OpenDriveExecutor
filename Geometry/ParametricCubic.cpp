/*
 * ParametricCubic.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */
#include <boost/lexical_cast.hpp>

#include "ParametricCubic.h"


ParametricCubic::ParametricCubic(pugi::xml_node node) : Geometry(node.parent())
{
	for (pugi::xml_attribute attr: node.attributes())
	{
		if(std::string(attr.name()) == std::string("aU"))
			aU = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("bU"))
			bU = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("cU"))
			cU = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("dU"))
			dU = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("aV"))
			aV = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("bV"))
			bV = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("cV"))
			cV = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("dV"))
			dV = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("range"))
			range = boost::lexical_cast<double>(attr.value());
	}
}

ParametricCubic::~ParametricCubic()
{
	// TODO Auto-generated destructor stub
}

OpenDriveMath::Point ParametricCubic::getPosition(double os)
{
	double offset = os-s;

	return OpenDriveMath::Point(aU + bU*offset + cU*offset*offset + dU*offset*offset*offset,
			aV + bV*offset + cV*offset*offset + dV*offset*offset*offset);
}
