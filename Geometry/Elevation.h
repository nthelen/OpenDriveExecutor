/*
 * Elevation.h
 *
 *  Created on: Feb 11, 2016
 *      Author: thelenn1
 */

#ifndef GEOMETRY_ELEVATION_H_
#define GEOMETRY_ELEVATION_H_

#include "../XML/pugixml.hpp"

struct ElevationData
{
	double s;
	double a;
	double b;
	double c;
	double d;
};

class Elevation
{
public:


	Elevation(pugi::xml_node node);
	virtual ~Elevation();
	double getElevation(double as);
	ElevationData getData();
protected:
	ElevationData data;
};

#endif /* GEOMETRY_ELEVATION_H_ */
