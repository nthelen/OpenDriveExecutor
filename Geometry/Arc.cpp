/*
 * Arc.cpp
 *
 *  Created on: Jan 27, 2016
 *      Author: thelenn1
 */

#include "Arc.h"
#include <boost/geometry/geometries/point.hpp>
#include <math.h>

#include "Geometry.h"
#include "../constants.h"

Arc::Arc(pugi::xml_node node) : Geometry(node.parent()) , center(getRadius()*cos(heading),getRadius()*sin(heading))
{

	for (pugi::xml_attribute attr: node.attributes())
	{
		if(attr.name() == std::string("curvature"))
			curvature = boost::lexical_cast<double>(attr.value());
	}
	//x  =  h + r cosθ
	//y  =  k + r sinθ
	//center = OpenDriveMath::Point(getRadius()*cos(heading),getRadius()*sin(heading));

}

OpenDriveMath::Point Arc::getPosition(double os)
{
	double lHeading=heading+(s*curvature);
	//x  =  h + r cosθ
	//y  =  k + r sinθ
	return OpenDriveMath::Point(center.x+getRadius()*cos(lHeading),center.y+getRadius()*sin(lHeading));
}

double Arc::getRadius()
{
	return 1.0/curvature;
}

