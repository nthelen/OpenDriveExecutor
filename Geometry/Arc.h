/*
 * Arc.h
 *
 *  Created on: Jan 27, 2016
 *      Author: thelenn1
 */

#ifndef ARC_H_
#define ARC_H_

#include "Geometry.h"
#include "../Math/Box.h"

class Arc: public Geometry {
public:
	Arc(pugi::xml_node);
	bool isInBox(OpenDriveMath::Point point);
	OpenDriveMath::Box makeBox(OpenDriveMath::Point center,double s, double width);
	double curvature;
	OpenDriveMath::Point center;
	double getRadius();
	double getCenter();
	OpenDriveMath::Point getPosition(double os);
};

#endif /* ARC_H_ */
