/*
 * Crossfall.h
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#ifndef CROSSFALL_H_
#define CROSSFALL_H_

#include <boost/lexical_cast.hpp>

#include "LateralProfile.h"
#include "../XML/pugixml.hpp"

enum Side { SideLeft = 0,SideRight=1, SideBoth=2};

class Crossfall: public LateralProfile
{
	public:
		Crossfall(pugi::xml_node);
		double getElevation(double t);
		double a;
		double b;
		double c;
		double d;
		Side side;
};

#endif /* CROSSFALL_H_ */
