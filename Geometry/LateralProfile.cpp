/*
 * LateralProfile.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#include <boost/lexical_cast.hpp>
#include "LateralProfile.h"


LateralProfile::LateralProfile(pugi::xml_node node)
{
	for (pugi::xml_attribute attr: node.attributes())
	{
		if(std::string(attr.name()) == std::string("s"))
			s = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("a"))
			a = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("b"))
			b = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("c"))
			c = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("d"))
			d = boost::lexical_cast<double>(attr.value());
	}
}

double LateralProfile::getS()
{
	return s;
}

double LateralProfile::getElevation(double t){ return 0,0; };
