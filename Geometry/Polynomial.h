/*
 * Polynomial.h
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#ifndef POLYNOMIAL_H_
#define POLYNOMIAL_H_

#include "Geometry.h"

class Polynomial: public Geometry
{
public:
	double a,b,c,d;
	double range;

	Polynomial(pugi::xml_node);
	OpenDriveMath::Point getPosition(double os);
};

#endif /* POLYNOMIAL_H_ */
