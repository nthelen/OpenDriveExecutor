/*
 * Line.cpp
 *
 *  Created on: Feb 4, 2016
 *      Author: thelenn1
 */

#include "Line.h"
#include "../Constants.h"
#include <CMath>

Line::Line(pugi::xml_node node) : Geometry(node.parent())
{
	heading = 0;

}

Line::~Line()
{

}

OpenDriveMath::Point Line::getPosition(double os)
{
	double offset = os-s;

	return OpenDriveMath::Point(offset,0);
}


