/*
 * LateralProfile.h
 *
 *  Created on: Feb 3, 2016
 *      Author: thelenn1
 */

#ifndef LATERALPROFILE_H_
#define LATERALPROFILE_H_

#include "../XML/pugixml.hpp"

class LateralProfile
{
public:
	double getS();
	virtual double getElevation(double t);
	LateralProfile(pugi::xml_node);
protected:
	double a;
	double b;
	double c;
	double d;
	double s;


};

#endif /* LATERALPROFILE_H_ */
