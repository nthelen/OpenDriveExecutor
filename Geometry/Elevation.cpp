/*
 * Elevation.cpp
 *
 *  Created on: Feb 11, 2016
 *      Author: thelenn1
 */
#include <boost/lexical_cast.hpp>

#include "Elevation.h"

Elevation::Elevation(pugi::xml_node node)
{
	for (pugi::xml_attribute attr: node.attributes())
	{
		if(std::string(attr.name()) == std::string("a"))
			data.a = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("b"))
			data.b = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("c"))
			data.c = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("d"))
			data.d = boost::lexical_cast<double>(attr.value());
		if(std::string(attr.name()) == std::string("s"))
			data.s = boost::lexical_cast<double>(attr.value());
	}
}

Elevation::~Elevation()
{

}

double Elevation::getElevation(double as)
{
	double ds = as-data.s;

	return data.a+data.b*ds+data.c*ds*ds+data.d*ds*ds*ds;
}

ElevationData Elevation::getData()
{
	return data;

}
