/*
 * Geometry.h
 *
 *  Created on: Jan 27, 2016
 *      Author: thelenn1
 */

#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#include <vector>
#include <memory>
#include <functional>
#include <utility>

#include "../XML/pugixml.hpp"
#include "../Math/Box.h"
#include "../Math/LinearLine.h"
#include "../Math/Point.h"
#include "../Math/Point.h"
#include "../UserData/UserData.h"

class Geometry {
public:
	Geometry(pugi::xml_node);

	//virtual OpenDriveMath::Box makeBox(OpenDriveMath::Point center,double s, double width);
	Geometry(double s,OpenDriveMath::Point center,double heading,double length);
	OpenDriveMath::Box getBox();
	void updateMinMax(OpenDriveMath::Box box);
	bool isInBox(OpenDriveMath::Point point);
	double getS() { return s; };
	double getHeading(double os);

	virtual OpenDriveMath::Point getPosition(double os);
	OpenDriveMath::Box makeBox(double os, std::function< std::pair<double,double>(double)> laneLength);
	std::vector<OpenDriveMath::Box> makeBoxes(std::function< std::pair<double,double>(double)> laneLength);
	OpenDriveMath::Point getPosition(double s, double t);

	bool testPosition(OpenDriveMath::Point point,
			double s,std::function< std::pair<double,double>(double)> laneLength);

	std::pair<double,double> getRoadPosition(OpenDriveMath::Point point,double os,
			std::function< std::pair<double,double>(double)> laneLength);
	Geometry() = delete;

protected:
	std::shared_ptr<OpenDriveMath::Point> center;
	double s;
	double heading;
	double length;
	std::shared_ptr<OpenDriveMath::Point> min;
	std::shared_ptr<OpenDriveMath::Point> max;
	std::shared_ptr<OpenDriveMath::Box> fullBox;

	std::vector<OpenDriveMath::Box> boxes;


};

#endif /* GEOMETRY_H_ */
