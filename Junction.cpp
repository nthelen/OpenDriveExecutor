/*
 * Junction.cpp
 *
 *  Created on: Feb 6, 2016
 *      Author: thelenn1
 */
#include <boost/lexical_cast.hpp>

#include "Junction.h"

Junction::Junction(pugi::xml_node node)
{
	// TODO Auto-generated constructor stub
	for (pugi::xml_attribute attr: node.attributes())
	{
		if (std::string(attr.name()) == std::string("name"))
			data.name = std::string(attr.value());
		if (std::string(attr.name()) == std::string("id"))
			data.id = std::string(attr.value());
	}

	for (pugi::xml_node child: node.children())
	{
		if(std::string(child.name()) == std::string("connection"))
			data.connections.push_back(std::shared_ptr<Connection>(new Connection(node)));
		if(std::string(child.name()) == std::string("priority"))
			data.priorities.push_back(std::shared_ptr<Priority>(new Priority(node)));
		if(std::string(child.name()) == std::string("controller"))
			data.controllers.push_back(std::shared_ptr<Controller>(new Controller(node)));

	}
}

std::vector<std::string> Junction::getConnectingRoads(std::string roadString) const
{
	std::vector<std::string> roads;
	for(std::shared_ptr<Connection> connection : data.connections)
	{
		if(connection->getConnectionData().incomingRoad == roadString)
			roads.push_back(connection->getConnectionData().connectingRoad);
	}
}

