/*
 * Connection.h
 *
 *  Created on: Feb 6, 2016
 *      Author: thelenn1
 */

#ifndef CONNECTION_H_
#define CONNECTION_H_

#include <vector>
#include <memory>
#include "Roads/Lane.h"
#include "XML/pugixml.hpp"
#include <utility>

enum ContactPoint { START, END };

struct LaneLink
{
	LaneLink(int afrom, int ato) : from(afrom), to(ato){}
	int from;
	int to;
};

struct ConnectionData
{
	std::vector<LaneLink> laneLinks;
	std::string id;
	std::string incomingRoad;
	std::string connectingRoad;
	ContactPoint contactPoint;
};



class Connection {
public:
	Connection(pugi::xml_node node);
	const ConnectionData& getConnectionData() const;
protected:
	ConnectionData data;
private:
	Connection();
};

#endif /* CONNECTION_H_ */
