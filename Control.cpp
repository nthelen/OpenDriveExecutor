/*
 * Control.cpp
 *
 *  Created on: Feb 6, 2016
 *      Author: thelenn1
 */
#include <boost/lexical_cast.hpp>

#include "Control.h"

Control::Control(pugi::xml_node node)
{
	for (pugi::xml_attribute attr: node.attributes())
	{
		if (std::string(attr.name()) == std::string("signalId"))
			data.signalId = boost::lexical_cast<std::string>(attr.value());
		if (std::string(attr.name()) == std::string("type"))
			data.type = boost::lexical_cast<std::string>(attr.value());
	}
}

Control::~Control()
{

}

