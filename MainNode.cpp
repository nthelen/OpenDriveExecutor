/*
 * Roads.cpp
 *
 *  Created on: Feb 21, 2016
 *      Author: thelenn1
 */

#include "MainNode.h"
#include <vector>
#include <Map>
#include <Memory>
#include <set>
#include <functional>
#include "Constants.h"

MainNode::MainNode(std::string fileName) : quadTree(new QuadTree<TreeData>(OpenDriveMath::Box(OpenDriveMath::Point(-10000,-10000),OpenDriveMath::Point(10000,10000)), (int)Constants::MaxTreeItems))
{
	pugi::xml_document doc;

	pugi::xml_parse_result result = doc.load_file(fileName.c_str());

	try
	{
	if(result.status == pugi::status_ok)
	{
		mainParse(doc.root());
	}

	else
	{
		throw std::runtime_error("Could not load " + fileName);
	}
	}
	catch (pugi::xpath_exception& e)
	{
		e.what();

	}
}

MainNode::MainNode(pugi::xml_node node) : quadTree(new QuadTree<TreeData>(OpenDriveMath::Box(OpenDriveMath::Point(-10000,-10000),OpenDriveMath::Point(10000,10000)), Constants::MaxTreeItems))
{
	mainParse(node);
}

void MainNode::mainParse(pugi::xml_node node)
{
	for (pugi::xml_node child: node.children())
	{
		o = std::bind(&MainNode::findObject, this, std::placeholders::_1);
		s = std::bind(&MainNode::findSignal, this, std::placeholders::_1);

		searchOpenDrive(node);

		buildTree(OpenDriveMath::Box(OpenDriveMath::Point(-10000,-10000),OpenDriveMath::Point(10000,10000)));
	}

}

void MainNode::getSignals(pugi::xml_node node)
{
	if(!node.empty())
	{
		for (pugi::xml_node child: node.children())
		{
			if(std::string(child.name())=="signal")
			{
				Signal* signal = new Signal(child);
				signals[std::string(signal->getData().id)] = std::shared_ptr<Signal>(signal);
			}
			else getSignals(child);
		}
	}
}

const RoadData& MainNode::getRoad(std::string road)
{
	return roads[road]->getData();
}

void MainNode::getObjects(pugi::xml_node node)
{
	if(!node.empty())
	{
		for (pugi::xml_node child: node.children())
		{
			if(std::string(child.name())=="object")
			{
				Object* object = new Object(child);
				objects[std::string(object->getData().id)] = std::shared_ptr<Object>(object);
			}
			else getObjects(child);
		}
	}
}

void MainNode::searchOpenDrive(pugi::xml_node node)
{
	getSignals(node);
	getObjects(node);

	if(!node.empty())
	{
		for (pugi::xml_node child: node.children())
		{

			if(std::string(child.name())=="road")
			{
				Road* road = new Road(child,o,s);
				roads[std::string(road->getData().id)] = std::shared_ptr<Road>(road);
			}
			if(std::string(child.name())=="junction")
			{
				Junction* junction = new Junction(child);
				junctions[std::string(junction->getData().id)] = std::shared_ptr<Junction>(junction);
			}
			if(std::string(child.name())=="controller")
			{
				Controller* controller = new Controller(child);
				controllers[controller->getData().id] = std::shared_ptr<Controller>(controller);
			}
			searchOpenDrive(child);
		}
	}
	else
	{
		return;
	}
}

void MainNode::buildTree(OpenDriveMath::Box range)
{
	std::vector<TreeData> treeNodes;
	for(std::pair<std::string, std::shared_ptr<Road> > iterator : roads)
	{
		std::vector<TreeData> box = iterator.second->getGeometryAreas();
		for(TreeData b : box)
		{
			TreeData* treeData = new TreeData(b);
			quadTree->insert(treeData);
		}
	}
}

ObjectData MainNode::findObject(std::string id)
{
	if(objects.find(id) != objects.end())
	{
		Object object = *objects[id];
		return object.getData();
	}
	else throw std::runtime_error("Error finding object");
}

SignalData MainNode::findSignal(std::string id)
{
	if(signals.find(id) != signals.end())
	{
		Signal signal = *signals[id];
		return signal.getData();
	}
	else throw std::runtime_error("Error finding signal");
}

UserRoadData MainNode::laneToRoad(UserLaneData ldata) const
{
	return (roads.find(std::string(ldata.roadId)))->second->getData().
			lanes.equal_range(ldata.roadOffset).first->second->getRoadDataFromLaneData(ldata);
}

void MainNode::createConnectors()
{
	for (std::pair<std::string, std::shared_ptr<Road> > road : roads)
	{
		if(road.second->getData().predecessor.getData().elementType == ElementTypes::JunctionElement)
		{
			roadNodes[road.first] = junctions[std::string(road.second->getData().predecessor.getData().elementId)]->getConnectingRoads(road.second->getData().id);
		}
	}
}



UserRoadData MainNode::positionToRoad(UserPointData data)
{
	UserRoadData roadData;

	std::vector< TreeData > items = quadTree->getItems(OpenDriveMath::Point(data.x,data.y));
	std::pair<double, double> roadPosition;
	std::pair<double, double> currentRoadPosition = std::pair<double,double>(10000,10000);

	for(TreeData item : items)
	{
		roadPosition = item.geometry->getRoadPosition(OpenDriveMath::Point(data.x,data.y),item.geometry->getS(),
				std::bind(&Road::getLaneLength, item.road,std::placeholders::_1));

		if(roadPosition.second < currentRoadPosition.second)
		{
			currentRoadPosition = roadPosition;
			roadData.roadOffset = currentRoadPosition.second;
			roadData.sPosition = roadPosition.first;
			roadData.road = item.road->getData().id;
		}
	}
	return roadData;
}


UserRoadData MainNode::routeToRoad(UserRouteData path)
{
	UserRoadData roadData;
	roadData.roadOffset = path.roadOffset;
	roadData.sPosition = path.sPosition;
	roadData.road = path.road;

	return roadData;
}

UserPointData MainNode::roadToPoint(UserRoadData userRoadData)
{
	return roads[userRoadData.road]->getPosition(userRoadData.sPosition,userRoadData.roadOffset);
}

UserLaneData MainNode::roadToLane(UserRoadData userRoadData)
{

	return roads[userRoadData.road]->getLaneData(userRoadData);
}

UserRouteData MainNode::roadToRoute(UserRoadData roadData,OpenDrivePath path)
{
	UserRouteData routeData;

	routeData.road = roadData.road;
	routeData.roadOffset = roadData.roadOffset;
	routeData.sPosition = roadData.sPosition;
	routeData.totalPosition = path.getPathDistance(roadData.road, roadData.sPosition);

	return routeData;
}
